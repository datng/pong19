from PyQt5.QtCore import QObject, QTimer
from PyQt5.QtWidgets import QApplication

from networking.networking import p19Client_Networking
from gui.gui import p19clientGUI

class Client(QObject):
    def __init__(self, parent=None):
        super(Client, self).__init__(parent)
        self.m_Network = p19Client_Networking(self)
        self.m_GUI = p19clientGUI()

        # This timer is necessary for the cancellation of the process inside the terminal
        # Read the explanation here
        # https://machinekoder.com/how-to-not-shoot-yourself-in-the-foot-using-python-qt/
        self.m_Timer = QTimer()
        self.m_Timer.timeout.connect(lambda: None)
        self.m_Timer.start(100)

        # Connections between the components
        self.m_GUI.CreateMatch.connect(self.m_Network.CreateMatch)
        self.m_GUI.JoinMatch.connect(self.m_Network.JoinMatch)
        self.m_GUI.MatchFeatures.connect(self.m_Network.MatchFeatures)
        self.m_GUI.ReportKey.connect(self.m_Network.ReportKey)
        self.m_GUI.LeavingMatch.connect(self.m_Network.LeavingMatch)
        self.m_GUI.ConnectTCP.connect(self.m_Network.ConnectTCP)
        self.m_GUI.IAmReady.connect(self.m_Network.Ready)
        self.m_GUI.StateChanged.connect(self.m_Network.UpdateState)
        self.m_GUI.CreateMatchFeatures.connect(
            self.m_Network.CreateMatchFeatures)

        self.m_Network.Dimension.connect(self.m_GUI.data.Dimension)
        self.m_Network.GameEnded.connect(self.m_GUI.GameEnded)
        self.m_Network.MatchCreated.connect(self.m_GUI.MatchCreated)
        self.m_Network.AddMatches.connect(self.m_GUI.AddMatches)
        self.m_Network.RemoveMatches.connect(self.m_GUI.RemoveMatches)
        self.m_Network.FeatureList.connect(self.m_GUI.Update_Feature_List)
        self.m_Network.MatchStarted.connect(self.m_GUI.MatchStarted)
        self.m_Network.UpdateServers.connect(self.m_GUI.UpdateServers)
        self.m_Network.UpdateConnection.connect(self.m_GUI.UpdateConnection)

        self.m_Network.ScoreUpdate.connect(self.m_GUI.ScoreUpdate)
        self.m_Network.RoundUpdate.connect(self.m_GUI.RoundUpdate)
        self.m_Network.UpdateBall.connect(self.m_GUI.UpdateBall)
        self.m_Network.UpdatePlayer.connect(self.m_GUI.UpdatePlayer)

        self.m_Network.NotUnderstood.connect(self.m_GUI.NotUnderstood)
        self.m_Network.FailedToCreate.connect(self.m_GUI.FailedToCreate)
        self.m_Network.FailedToJoin.connect(self.m_GUI.FailedToJoin)
        self.m_Network.GameNotExist.connect(self.m_GUI.GameNotExist)
        self.m_Network.DisconnectingYou.connect(self.m_GUI.DisconnectingYou)

        self.m_GUI.show()


if __name__ == '__main__':
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    import sys
    app = QApplication(sys.argv)
    client = Client()
    sys.exit(app.exec_())
