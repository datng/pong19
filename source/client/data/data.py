from PyQt5.QtCore import QObject, QSize, pyqtSignal, QTimer
from enum import Enum


Striker_Length = 40
Striker_Width = 8

period = 1/60


class data(QObject):
    DIMWidth = 640
    DIMHeight = 480

    Send_Update = pyqtSignal(float, float, float, float, int, int)

    def __init__(self, parent=None):
        super().__init__()

        # Ball
        self.ball_pos_x = None
        self.ball_pos_y = None
        self.ball_vel_x = None
        self.ball_vel_y = None

        # Player 1 Assumption Player 1 always left
        self.player1_pos_x = None
        self.player1_pos_y = None
        self.player1_vel_x = None
        self.player1_vel_y = None
        self.player1_score = 0

        # Player 2
        self.player2_pos_x = None
        self.player2_pos_y = None
        self.player2_vel_x = None
        self.player2_vel_y = None
        self.player2_score = 0

        # Timer for interpolation
        self.timer_inter = QTimer()
        self.timer_inter.setSingleShot(False)
        self.timer_inter.timeout.connect(self.interpolate)
        self.timer_inter.start(period*1000)

    def Size(self):
        return QSize(self.DIMWidth, self.DIMHeight)

    def relativate_values_and_send(self):
        if self.ball_pos_x != None and self.player1_pos_x != None and self.player2_pos_x != None:
            rel_ball_pos_x = self.ball_pos_x/self.DIMWidth
            rel_ball_pos_y = self.ball_pos_y/self.DIMHeight

            rel_player1_pos_y = self.player1_pos_y/self.DIMHeight
            rel_player2_pos_y = self.player2_pos_y/self.DIMHeight

            self.Send_Update.emit(rel_player1_pos_y, rel_player2_pos_y, rel_ball_pos_x,
                                  rel_ball_pos_y, self.player1_score, self.player2_score)

    def interpolate(self):
        if self.ball_pos_x != None and self.player1_pos_x != None and self.player2_pos_x != None:
            # Ball
            self.ball_pos_x = self.ball_pos_x + self.ball_vel_x * period
            self.ball_pos_y = self.ball_pos_y + self.ball_vel_y * period

            if self.ball_pos_y <= 0:
                self.ball_pos_y = 0
                self.ball_vel_y = 0 - self.ball_vel_y

            elif self.ball_pos_y >= self.DIMHeight:
                self.ball_pos_y = self.DIMHeight
                self.ball_vel_y = 0 - self.ball_vel_y

            if (self.ball_pos_y >= (self.player1_pos_y - Striker_Length/2)) and (self.ball_pos_y <= (self.player1_pos_y + Striker_Length/2)) and (self.ball_pos_x <= Striker_Width):
                self.ball_pos_x = Striker_Width + 0
                self.ball_vel_x = 0 - self.ball_vel_x

            elif (self.ball_pos_y >= (self.player2_pos_y - Striker_Length/2)) and (self.ball_pos_y <= (self.player2_pos_y + Striker_Length/2)) and (self.ball_pos_x >= self.DIMWidth - Striker_Width):

                self.ball_pos_x = self.DIMWidth - Striker_Width - 0
                self.ball_vel_x = 0 - self.ball_vel_x

            # Player 1

            self.player1_pos_y = self.player1_pos_y + self.player1_vel_y * period

            if self.player1_pos_y <= Striker_Length/2:
                self.player1_pos_y = Striker_Length/2
                self.player1_vel_y = 0

            elif self.player1_pos_y >= self.DIMHeight - Striker_Length/2:
                self.player1_pos_y = self.DIMHeight - Striker_Length/2
                self.player1_vel_y = 0

            # Player 2

            self.player2_pos_y = self.player2_pos_y + self.player2_vel_y * period

            if self.player2_pos_y <= Striker_Length/2:
                self.player2_pos_y = Striker_Length/2
                self.player2_vel_y = 0

            elif self.player2_pos_y >= self.DIMHeight - Striker_Length/2:
                self.player2_pos_y = self.DIMHeight - Striker_Length/2
                self.player2_vel_y = 0

        self.relativate_values_and_send()

    def handle_input_ball(self, ball_no: str, pos_x: int, pos_y: int, vel_x: int, vel_y: int):
        self.ball_pos_x = pos_x + 4
        self.ball_pos_y = pos_y + 4

        self.ball_vel_x = vel_x
        self.ball_vel_y = vel_y

    def handle_input_player(self, player_id: str, pos_x: int, pos_y: int, vel_x: int, vel_y: int):
        if player_id == "1":

            self.player1_pos_x = pos_x
            self.player1_pos_y = pos_y + 60

            self.player1_vel_x = vel_x
            self.player1_vel_y = vel_y

        elif player_id == "2":

            self.player2_pos_x = pos_x
            self.player2_pos_y = pos_y + 60

            self.player2_vel_x = vel_x
            self.player2_vel_y = vel_y

    def handle_score(self, player_id: str, score: int):

        if player_id == "1":
            self.player1_score = score

        elif player_id == "2":
            self.player2_score = score

    def Dimension(self, DIMS):
        self.DIMWidth = int(DIMS[2])
        self.DIMHeight = int(DIMS[3])

    def ResetData(self):

        # Ball
        self.ball_pos_x = None
        self.ball_pos_y = None
        self.ball_vel_x = None
        self.ball_vel_y = None

        # Player 1 Assumption Player 1 always left
        self.player1_pos_x = None
        self.player1_pos_y = None
        self.player1_vel_x = None
        self.player1_vel_y = None
        self.player1_score = 0

        # Player 2
        self.player2_pos_x = None
        self.player2_pos_y = None
        self.player2_vel_x = None
        self.player2_vel_y = None
        self.player2_score = 0
