from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QFrame, QVBoxLayout, QHBoxLayout, QSpacerItem, QSizePolicy, QLabel, QLayout
from PyQt5.QtGui import QPainter, QPen, QPainterPath, QPixmap


BallSize = 8
RacketWidth = 8
RacketHeight = 120
FenceWidth = 4


class Ball(QFrame):
    def __init__(self, Parent):
        super(Ball, self).__init__(Parent)
        self.setStyleSheet("background-color:gray;")
        self.resize(BallSize, BallSize)

    def Move(self, X, Y, Size):
        self.X = X
        self.Y = Y
        Width = Size.width() - self.width()
        Height = Size.height() - self.height()

        self.move(Width*X, Height*Y)


class Racket(QFrame):
    def __init__(self, Parent, Left, Color):
        super(Racket, self).__init__(Parent)
        self.setStyleSheet("background:rgb(" + Color + ");")
        self.Left = Left
        self.resize(RacketWidth, RacketHeight)

    def Move(self, Position, Size):
        self.Position = Position
        if (self.Left):
            self.move(0,
                      Size.height()*Position-self.height()/2)
        else:
            self.move(Size.width()-RacketWidth, Size.height()
                      * Position-self.height()/2)


class Table(QFrame):
    def __init__(self, Parent, ColorLeft, ColorRight, Size):
        super(Table, self).__init__(Parent)
        self.setStyleSheet("border: 1px solid black;")

        self.pixmap = QPixmap(
            'images/background.png').scaled(self.width(), self.height())

        VSTable = QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)
        BottomLayout = QHBoxLayout()
        BottomLayout.addItem(VSTable)
        self.setLayout(BottomLayout)
        self.resize(Size.width(), Size.height())
        self.setFixedSize(Size.width(), Size.height())

        self.Left = Racket(self, True, ColorLeft)
        self.Ball = Ball(self)
        self.Right = Racket(self, False, ColorRight)

    def Update(self, PosLeft, PosRight, X, Y):
        self.Left.Move(PosLeft, self.size())
        self.Ball.Move(X, Y, self.size())
        self.Right.Move(PosRight, self.size())

    def paintEvent(self, PaintEvent):
        Painter = QPainter(self)
        Pen = QPen(Qt.black, FenceWidth,
                   Qt.DashLine, Qt.FlatCap, Qt.MiterJoin)
        Painter.drawPixmap(0, 0, self.width(), self.height(), self.pixmap)
        Painter.setPen(Pen)
        Painter.drawLine(self.width()/2, 0, self.width()/2, self.height())


class Match(QWidget):
    def __init__(self, Parent, ColorLeft, ColorRight, Size):
        super(Match, self).__init__(Parent)
        Layout = QVBoxLayout()
        Layout.setSizeConstraint(QLayout.SetMaximumSize)

        TopLayout = QHBoxLayout()

        HSLeft = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.ScoreLeft = QLabel("0")
        self.ScoreLeft.setAlignment(
            Qt.AlignRight | Qt.AlignTrailing | Qt.AlignVCenter)
        HSCenterLeft = QSpacerItem(
            0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.Score = QLabel(":")
        self.Score.setAlignment(Qt.AlignCenter)
        self.ScoreRight = QLabel("0")
        Font = self.ScoreLeft.font()
        HSCenterRight = QSpacerItem(
            0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        Font.setPointSize(20)
        Font.setBold(True)
        self.ScoreLeft.setFont(Font)
        self.Score.setFont(Font)
        self.ScoreRight.setFont(Font)
        HSRight = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        TopLayout.addItem(HSLeft)
        TopLayout.addWidget(self.ScoreLeft)
        TopLayout.addItem(HSCenterLeft)
        TopLayout.addWidget(self.Score)
        TopLayout.addItem(HSCenterRight)
        TopLayout.addWidget(self.ScoreRight)
        TopLayout.addItem(HSRight)

        Layout.addItem(TopLayout)

        self.Table = Table(self, ColorLeft, ColorRight, Size)
        self.Update(0.5, 0.5, 0.5, 0.5, 0, 0)
        Layout.addWidget(self.Table)

        self.setLayout(Layout)

    def RoundUpdate(self, Round):
        self.Score.setText(Round)

    def Update(self, PosLeft, PosRight, X, Y, ScoreLeft, ScoreRight):
        self.Table.Update(PosLeft, PosRight, X, Y)
        self.ScoreLeft.setText(str(ScoreLeft))
        self.ScoreRight.setText(str(ScoreRight))
