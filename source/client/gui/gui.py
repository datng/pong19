from PyQt5.QtCore import Qt, QSize, pyqtSignal
from PyQt5.QtWidgets import QApplication, QWidget, QStackedWidget, QVBoxLayout, QSpacerItem, QSizePolicy, QHBoxLayout, QMainWindow, QLabel, QListWidget, QListWidgetItem, QPushButton, QAction, QLineEdit, QMessageBox, QComboBox, QGroupBox, QGridLayout, QCheckBox, QStatusBar
from PyQt5.QtGui import QCloseEvent

from enum import Enum

from .match_gui import Match
from data.data import data

StatusBarDuration = 4000


class States(Enum):
    Lobby = 0
    LobbyCreated = 1
    MatchStarted = 2
    Match = 3

    def __int__(self):
        return self.value


class p19clientGUI (QMainWindow):

    # Signals
    CreateMatch = pyqtSignal(str)
    JoinMatch = pyqtSignal(str, list)
    MatchFeatures = pyqtSignal(str)
    ConnectTCP = pyqtSignal(str)
    StateChanged = pyqtSignal(int)
    CreateMatchFeatures = pyqtSignal(list)

    # Signal for Match context:
    ReportKey = pyqtSignal(str)
    LeavingMatch = pyqtSignal(str)
    IAmReady = pyqtSignal()

    features = []

    # Game state
    State = States.Lobby

    def __init__(self, parent=None):
        super(p19clientGUI, self).__init__(parent)

        self.InitUI()

        self.SetState(States.Lobby)
        self.Connected = False

    def InitUI(self):
        self.Stack = QStackedWidget()
        self.setCentralWidget(self.Stack)

        self.Lobby = QWidget()
        self.Lobby.setLayout(QHBoxLayout())

        self.Stack.addWidget(self.Lobby)

        # Available servers group box initialization:
        self.GroupBox1 = QGroupBox(self.Lobby)
        self.GroupBox1.setTitle("Server")
        Layout = QVBoxLayout()
        self.GroupBox1.setLayout(Layout)
        Layout.addWidget(QLabel('Available Servers:'))
        self.available_servers = QListWidget()
        Layout.addWidget(self.available_servers)
        ConnectButton = QPushButton("Connect")
        ConnectButton.setToolTip(
            'choose a server from the list and then click Connect')
        ConnectButton.clicked.connect(self.Connect_clicked)
        self.available_servers.itemDoubleClicked.connect(self.ServerClicked)
        Layout.addWidget(ConnectButton)
        self.Lobby.layout().addWidget(self.GroupBox1)

        # Join match group box initialization:
        self.GroupBox2 = QGroupBox(self.Lobby)
        self.GroupBox2.setTitle("Join")
        Layout = QVBoxLayout()
        self.GroupBox2.setLayout(Layout)
        Layout.addWidget(QLabel('Available Matches:'))
        self.available_match_list = QListWidget()
        self.available_match_list.currentItemChanged.connect(
            self.show_features_selected)
        Layout.addWidget(self.available_match_list)
        Layout.addWidget(QLabel("Features:"))
        self.features_list = QListWidget()
        Layout.addWidget(self.features_list)
        self.Dropdown = QComboBox()
        self.Dropdown.addItems(
            ["Green", "Red", "Blue", "Yellow"])
        Layout.addWidget(self.Dropdown)
        Join_button = QPushButton('Join')
        Join_button.setToolTip(
            'choose a match from the list and then click join')
        Join_button.clicked.connect(self.join_clicked)
        Layout.addWidget(Join_button)
        self.Lobby.layout().addWidget(self.GroupBox2)

        # Create match group box initialization:
        GroupBox3 = QGroupBox(self.Lobby)
        GroupBox3.setTitle("Create")
        Layout = QVBoxLayout()
        GroupBox3.setLayout(Layout)
        Layout.addWidget(QLabel("Enter Match Name: "))
        self.textbox = QLineEdit()
        Layout.addWidget(self.textbox)
        Layout.addWidget(QLabel("Add Features:"))
        self.feature_1 = QCheckBox("Best of 3")
        Layout.addWidget(self.feature_1)
        self.create_button = QPushButton('Create')
        self.create_button.setToolTip(
            'click here to create a match')
        self.create_button.clicked.connect(self.create_clicked)
        Layout.addWidget(self.create_button)
        Layout.addItem(QSpacerItem(
            0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding))
        self.quit_button = QPushButton("Quit")
        self.quit_button.clicked.connect(self.quit_clicked)
        Layout.addWidget(self.quit_button)
        self.Lobby.layout().addWidget(GroupBox3)

        self.StatusBar = QStatusBar()
        self.setStatusBar(self.StatusBar)

        self.resize(QSize(1000, 500))
        self.setWindowTitle("Pong")

        # Init Data Object
        self.data = data(self)

    def SetState(self, State):
        self.State = State
        self.data.Game_State = self.State
        self.StateChanged.emit(int(State))

    def closeEvent(self, Event):
        if (self.State == States.Lobby or self.State == States.LobbyCreated):
            Event.accept()
        else:
            self.GameEnded("", True)
            Event.ignore()

    def ServerClicked(self, Item):
        self.ConnectTCP.emit(Item.text())
    # Functions for Buttons

    def Connect_clicked(self):
        if (self.available_servers.currentItem() == None):
            self.StatusBar.showMessage(
                "Please choose a server.", StatusBarDuration)
        else:
            self.ConnectTCP.emit(self.available_servers.currentItem().text())

    def join_clicked(self):

        if (self.available_match_list.currentItem() == None):
            self.StatusBar.showMessage(
                "Please choose a match.", StatusBarDuration)
            return
        selected_match = self.available_match_list.currentItem().text()

        Index = self.Dropdown.currentIndex()
        if Index == 0:
            rgb_color = [0, 255, 0]
        elif Index == 1:
            rgb_color = [255, 0, 0]
        elif Index == 2:
            rgb_color = [0, 0, 255]
        elif Index == 3:
            rgb_color = [255, 255, 0]

        self.LobbySetEnabled(False)
        self.JoinMatch.emit(selected_match, rgb_color)

    def create_clicked(self):

        if (self.State == States.Lobby):
            if (not self.Connected):
                self.StatusBar.showMessage(
                    "ERROR: Connect to a server first.", StatusBarDuration)
                return

            match_name = self.textbox.text().replace(",", "").replace(" ", "")

            if match_name == "":

                self.StatusBar.showMessage(
                    "ERROR: please enter a name for the match", StatusBarDuration)
                return

            feature_list_create = []
            # TO DO: change names in list to our features (when implemented)
            # explanation / idea: empty list = basic game
            if self.feature_1.isChecked():
                feature_list_create.append("BEST_OF_3")

            if self.feature_1.isChecked() == False:
                feature_list_create = ["BASIC"]

            self.LobbySetEnabled(False)
            self.create_button.setText("Cancel")

            self.CreateMatchFeatures.emit(feature_list_create)

            self.CreateMatch.emit(match_name)
            # TODO / DISCUSS IN GROUP: add feature list to signal
        else:
            self.LeavingMatch.emit("Player has canceled match.")
            self.create_button.setText("Create")
            self.LobbySetEnabled(True)
            self.SetState(States.Lobby)
            self.StatusBar.showMessage("Match cancelled.", StatusBarDuration)

    def quit_clicked(self):
        if (self.State == States.LobbyCreated):
            self.LeavingMatch.emit("Player has cancelled match.")
            self.SetState(States.Lobby)
        self.close()

    # shoul get called if selected item in list changes
    def show_features_selected(self, Current, Previous):
        if (Current):
            self.MatchFeatures.emit(Current.text())
        else:
            self.Update_Feature_List(list())

    def Update_Feature_List(self, match_feature_list: list):

        self.features_list.clear()
        for x in match_feature_list:
            self.features_list.addItem(x)

    # Slots normals

    def GameEnded(self, message, fromGui=False):

        self.data.ResetData()

        # TO DO: close / end object match gui

        self.LobbySetEnabled(True)
        self.create_button.setText("Create")

        if (fromGui == False):
            Endscreen = QMessageBox(self)
            Endscreen.setText(message)
            Endscreen.setWindowTitle("Game Ended")
            Endscreen.show()
        else:
            self.LeavingMatch.emit("Player has quit.")

        self.Stack.setCurrentWidget(self.Lobby)
        self.SetState(States.Lobby)

    def MatchCreated(self):
        self.SetState(States.LobbyCreated)
        self.StatusBar.showMessage("created match", StatusBarDuration)

    def MatchStarted(self, port, playerList):
        self.Match = Match(
            self, playerList[1], playerList[3], self.data.Size())
        self.Stack.addWidget(self.Match)
        self.Stack.setCurrentWidget(self.Match)
        self.SetState(States.MatchStarted)

        self.resize(QSize(0, 0))

        self.data.Send_Update.connect(self.Match.Update)

    def ScoreUpdate(self, Player, Score):
        self.data.handle_score(Player, Score)

    def RoundUpdate(self, Round):
        self.Match.RoundUpdate("Round " + Round)

    def UpdateBall(self, Ball, X, Y, X_V, Y_V):
        self.data.handle_input_ball(Ball, X, Y, X_V, Y_V)

    def UpdatePlayer(self, Player, X, Y, X_V, Y_V):
        self.data.handle_input_player(Player, X, Y, X_V, Y_V)

    def AddMatches(self, Added: list):
        self.available_match_list.addItems(Added)

    def RemoveMatches(self, Removed: list):
        for R in Removed:
            Items = self.available_match_list.findItems(R, Qt.MatchExactly)
            if (len(Items)):
                self.available_match_list.takeItem(
                    self.available_match_list.row(Items[0]))

    def UpdateServers(self, Server, New: bool, Connected: bool):
        if (New):
            if (Connected):
                Item = QListWidgetItem(Server)
                self.available_servers.insertItem(0, Item)
                Item.setForeground(Qt.darkGreen)
            else:
                self.available_servers.addItem(Server)
        else:
            Item = self.available_servers.findItems(Server, Qt.MatchExactly)[0]
            self.available_servers.takeItem(self.available_servers.row(Item))

    def UpdateConnection(self, Server, Connected):
        self.Connected = Connected
        Items = self.available_servers.findItems(Server, Qt.MatchExactly)
        if (Connected):
            if (len(Items)):
                Item = Items[0]
                Item.setForeground(Qt.darkGreen)
                self.available_servers.takeItem(
                    self.available_servers.row(Item))
                self.available_servers.insertItem(0, Item)
                Item.setSelected(True)
            self.StatusBar.showMessage(
                "Connected to " + Server, StatusBarDuration)
        else:
            self.LobbySetEnabled(True)
            self.Stack.setCurrentWidget(self.Lobby)
            self.SetState(States.Lobby)
            if (len(Items)):
                Items[0].setForeground(Qt.black)
            self.StatusBar.showMessage(
                "Disconnected from " + Server, StatusBarDuration)
            self.available_match_list.clear()
            self.Update_Feature_List(list())
            self.Dropdown.setCurrentIndex(0)

    # SLots error

    def NotUnderstood(self):

        self.LobbySetEnabled(True)
        self.StatusBar.showMessage(
            "ERROR: server could not understand command", StatusBarDuration)

    def FailedToCreate(self, reason):

        self.SetState(States.Lobby)
        self.LobbySetEnabled(True)
        self.StatusBar.showMessage(
            reason, StatusBarDuration)

    def FailedToJoin(self, reason):

        self.LobbySetEnabled(True)
        self.StatusBar.showMessage(
            "ERROR: server could not join match; REASON: " + reason, StatusBarDuration)

    def GameNotExist(self, name):

        self.LobbySetEnabled(True)
        self.StatusBar.showMessage(
            "ERROR: " + name + " does not exist", StatusBarDuration)

    def DisconnectingYou(self, reason):

        self.LobbySetEnabled(True)
        self.Stack.setCurrentWidget(self.Lobby)
        self.SetState(States.Lobby)
        self.StatusBar.showMessage(
            "ERROR: you were banned from the server; REASON: " + reason, StatusBarDuration)

    def LobbySetEnabled(self, Enabled):
        self.GroupBox1.setEnabled(Enabled)
        self.GroupBox2.setEnabled(Enabled)

        self.textbox.setEnabled(Enabled)
        self.feature_1.setEnabled(Enabled)

    def keyPressEvent(self, Event):
        if (self.State != States.Lobby):
            if (Event.key() == Qt.Key_Space):
                if (self.State == States.MatchStarted):
                    self.IAmReady.emit()
                    self.SetState(States.Match)
                self.ReportKey.emit("SPACE")
            elif (Event.key() == Qt.Key_Up):
                self.ReportKey.emit("UP")
            elif (Event.key() == Qt.Key_Down):
                self.ReportKey.emit("DOWN")
