from PyQt5.QtCore import pyqtSignal, QObject, QTimer
from PyQt5.QtNetwork import QHostAddress, QTcpServer, QUdpSocket, QTcpSocket
from .messages import MessageBuilder, MessageChecker, MessageParser
from enum import Enum

import time
import re

ServerExpiration = 5000
BroadcastPeriod = 4000
ListMatchPeriod = 1000


class States(Enum):
    Lobby = 0
    LobbyCreated = 1
    MatchStarted = 2
    Match = 3

    def __int__(self):
        return self.value


class Server():
    def __init__(self, Map, Remove, IP, Name, Port):
        self.Address = IP
        self.Port = int(Port)
        self.ExpireTimer = QTimer()
        self.ExpireTimer.setSingleShot(True)
        self.ExpireTimer.timeout.connect(lambda: Remove(Name))
        self.ExpireTimer.start(ServerExpiration)


class p19Client_Networking(QObject):

    # Signals
    GameEnded = pyqtSignal(str)
    MatchCreated = pyqtSignal()
    AddMatches = pyqtSignal(list)
    RemoveMatches = pyqtSignal(list)
    MatchStarted = pyqtSignal(int, list)
    UpdateServers = pyqtSignal(str, bool, bool)
    UpdateConnection = pyqtSignal(str, bool)
    FeatureList = pyqtSignal(list)
    Dimension = pyqtSignal(list)

    ScoreUpdate = pyqtSignal(str, int)
    RoundUpdate = pyqtSignal(str)
    UpdateBall = pyqtSignal(str, int, int, int, int)
    UpdatePlayer = pyqtSignal(str, int, int, int, int)

    # Error signals
    NotUnderstood = pyqtSignal()
    FailedToCreate = pyqtSignal(str)
    FailedToJoin = pyqtSignal(str)
    GameNotExist = pyqtSignal(str)
    DisconnectingYou = pyqtSignal(str)

    # Variables
    State = States.Lobby
    SEQ = 0
    Matches = list()
    ConnectedServer = str()

    def __init__(self, parent=None):
        super(p19Client_Networking, self).__init__(parent)

        # Registry of all know seUpdateServersrvers.
        self.Servers = dict()

        # Message checker and builder
        self.MsgBuilder = MessageBuilder()
        self.MsgChecker = MessageChecker()
        self.MsgParser = MessageParser()

        # The socket that send the server broadcast.
        self.BroadcastSocket = QUdpSocket(self)
        self.BroadcastSocket.error.connect(self.__on_BroadcastSenderError)
        self.BroadcastSocket.readyRead.connect(self.__readBroadcast)

        # The UDP socket during playing game
        self.UdpSocket = QUdpSocket(self)
        self.UdpSocket.readyRead.connect(self.__readyReadUdp)

        # Send Broadcast once in 1 second
        self.__Broadcast()
        self.TimerBroadcast = QTimer()
        self.TimerBroadcast.start(BroadcastPeriod)
        self.TimerBroadcast.timeout.connect(self.__Broadcast)

        # The TcpSocket
        self.TcpSocket = QTcpSocket(self)
        self.TcpSocket.connected.connect(self.__sendHello)
        self.TcpSocket.readyRead.connect(self.__onReadyRead)
        self.TcpSocket.disconnected.connect(self.__onDisconnected)

    def __Broadcast(self):
        datagram = "DISCOVER_LOBBY\x00".encode("ASCII")
        self.BroadcastSocket.writeDatagram(
            datagram, QHostAddress.Broadcast, 54000)

    def __on_BroadcastSenderError(self, err):
        # TODO: print the error messages as strings
        # More infos at https://doc.qt.io/qt-5/qabstractsocket.html#SocketError-enum
        print("Broadcast Sender error: " + str(err))
        exit(1)

    def __readBroadcast(self):
        while self.BroadcastSocket.hasPendingDatagrams():
            receivedDatagram = self.BroadcastSocket.receiveDatagram()
            addr = receivedDatagram.senderAddress()
            data = receivedDatagram.data()
            data1 = str(data).split(" ")[1]
            Port = str(data1).split("\\")[0]
            #// To check messeages and addresse of server //#
            # portServer = receivedDatagram.senderPort()
            # print("[Broadcast] New UDP from " + addr.toString() + " " +
            #       str(portServer) + ": " + "Port to connect with Tcp: " + Port)
            IP = addr.toString()
            Name = IP.split(":")[3]
            S = self.Servers.get(Name)
            if (S != None):
                S.ExpireTimer.start(ServerExpiration)
            else:
                self.Servers[Name] = Server(
                    self.Servers, self.__serverTimeout, IP, Name, Port)
                # if connected server has same name as Name, change its displayed color to green
                self.UpdateServers.emit(
                    Name,
                    True,
                    self.ConnectedServer == Name)

    def __readyReadUdp(self):
        if self.UdpSocket.bytesAvailable() <= 1400:
            message, addr, port = self.UdpSocket.readDatagram(1400)
            if (addr.toString().split(":")[3] == self.MatchUDPAddress):
                message = str(message, encoding="ASCII")
                msgType = self.MsgChecker.CheckType(message)
                if msgType == MessageChecker.UPDATE_BALL:
                    self.__HandleUpdateBall(message)
                if msgType == MessageChecker.UPDATE_PLAYER:
                    self.__HandleUpdatePlayer(message)
            else:
                print("[Networking] UDP message received from unexpected server. ")
        else:  # Server is sending super long messages.
            print("[Networking] Server is trying to send very long message. "
                  + "Removing...")
            self.UdpSocket.disconnect()

    def __serverTimeout(self, Name):
        self.Servers.pop(Name)
        self.UpdateServers.emit(Name, False, False)

    def __onDisconnected(self):
        self.UpdateConnection.emit(
            self.ConnectedServer, False)
        self.Matches = list()
        self.ConnectedServer = str()
        print("[Networking] Server disconnected.")

    def __sendHello(self):
        message = self.MsgBuilder.HELLO("IKPLAYER", "BASIC")
        self.TcpSocket.write(message.encode("ASCII"))
        self.ConnectedServer = self.TcpSocket.peerAddress().toString()
        self.ConnectedServer = self.ConnectedServer.split(":")[3]
        self.UpdateConnection.emit(
            self.ConnectedServer, True)

    def __onReadyRead(self):
        if self.TcpSocket.bytesAvailable() <= 1024 * 1024:
            receivedbytes = str(self.TcpSocket.readAll(), encoding="ASCII")
            messagelist = receivedbytes.split("\x00")
            if(len(messagelist) != 1):
                messagelist = messagelist[:-1]
            else:
                return

            for message in messagelist:
                # figure out the type of message
                msgType = self.MsgChecker.CheckType(message)
                if msgType == MessageChecker.WELCOME:
                    self.__HandleWelcome(message)
                    self.ListGames()
                if msgType == MessageChecker.AVAILABLE_GAMES:
                    print("available games received")
                    self.__HandleAvailableGames(message)
                    if re.search("Pong", message) != None:
                        self.ListMatches()
                        self.TimerListMatch = QTimer()
                        self.TimerListMatch.timeout.connect(self.ListMatches)
                        self.TimerListMatch.start(ListMatchPeriod)
                    else:
                        print("Game Pong not exist")

                if msgType == MessageChecker.GAMES:
                    self.__HandleGames(message)
                if msgType == MessageChecker.GAME_ENDED:
                    self.GameEnded.emit(message)
                if msgType == MessageChecker.MATCH:
                    self.__HandleMatch(message)
                if msgType == MessageChecker.MATCH_CREATED:
                    self.TimerListMatch.stop()
                    print("[Lobby] " + message)
                    self.MatchCreated.emit()
                if msgType == MessageChecker.MATCH_JOINED:
                    self.__HandleMatchJoined(message)
                if msgType == MessageChecker.MATCH_STARTED:
                    self.__HandleMatchStarted(message)
                if msgType == MessageChecker.SCORE_UPDATE:
                    self.__HandleScoreUpdate(message)
                if msgType == MessageChecker.ROUND_UPDATE:
                    self.__HandleRoundUpdate(message)
                # Error Messages
                if msgType == MessageChecker.ERR_CMD_NOT_UNDERSTOOD:
                    self.NotUnderstood.emit()
                if msgType == MessageChecker.ERR_FAILED_TO_CREATE:
                    self.FailedToCreate.emit(message)
                if msgType == MessageChecker.ERR_FAILED_TO_JOIN:
                    self.FailedToJoin.emit(message)
                if msgType == MessageChecker.ERR_GAME_NOT_EXIST:
                    self.GameNotExist.emit(message)
                if msgType == MessageChecker.DISCONNECTING_YOU:
                    self.DisconnectingYou.emit(message)

        else:
            # Server is sending super long messages.
            print("[Networking] Server is trying to send very long message. "
                  + "Removing...")
            self.TcpSocket.disconnect()

    # Hanlde messages from server
    def __HandleAvailableGames(self, message: str):
        games = self.MsgParser.ParseAvailableGames(message)
        print("[AVAILABLE_GAMES] ", games)

    def __HandleGames(self, message: str):
        message = self.MsgParser.ParseGames(message)
        game = message[0]
        NewMatches = message[1]

        Added = list(set(NewMatches) - set(self.Matches))
        Removed = list(set(set(self.Matches)) - set(set(NewMatches)))

        self.Matches = NewMatches

        if (len(Added)):
            self.AddMatches.emit(Added)
        if (len(Removed)):
            self.RemoveMatches.emit(Removed)

        print("[GAMES] " + "[game]: " + game +
              " " + "[matches]: ", self.Matches)

    def __HandleMatch(self, message: str):
        message = self.MsgParser.ParseMatch(message)
        game = message[0]
        name = message[1]
        features = message[2]
        self.FeatureList.emit(features)
        print("[GAME] " + "[game]: " + game + " " +
              "[name]: " + name + " " + "[features]: ", features)

    def __HandleMatchJoined(self, message: str):
        message = message.split(" ")
        playerID = message[1]
        print("[MATCH_JOINED] " + "PlayerID: " + playerID)

    def __HandleMatchStarted(self, message: str):
        message = self.MsgParser.ParseMatchStarted(message)
        port = int(message[0])
        colorlist = message[1]
        self.MatchStarted.emit(port, colorlist)
        ip = self.TcpSocket.peerAddress().toString()
        self.MatchUDPAddress = ip.split(":")[3]
        self.MatchUDPPort = port
        self.ScoreUpdate.emit(str(1), int(0))
        self.ScoreUpdate.emit(str(2), int(0))

    def __HandleRoundUpdate(self, message: str):
        self.RoundUpdate.emit(self.MsgParser.ParseRoundUpdate(message))

    def __HandleScoreUpdate(self, message: str):
        message = self.MsgParser.ParseScoreUpdate(message)
        player = message[0]
        score = int(message[1])
        self.ScoreUpdate.emit(player, score)

    def __HandleUpdateBall(self, message: str):
        message = self.MsgParser.ParseUpdateBall(message)
        ball_no = message[0]
        x = int(message[1])
        y = int(message[2])
        x_v = int(message[3])
        y_v = int(message[4])
        self.UpdateBall.emit(ball_no, x, y, x_v, y_v)

    def __HandleUpdatePlayer(self, message: str):
        message = self.MsgParser.ParseUpdatePlayer(message)
        player = message[0]
        x = int(message[1])
        y = int(message[2])
        x_v = int(message[3])
        y_v = int(message[4])
        self.UpdatePlayer.emit(player, x, y, x_v, y_v)

    def __HandleWelcome(self, message: str):
        if re.search("DIMS", message) == None:
            features = self.MsgParser.ParseWelcome(message)
            print("[Networking] WELCOME command detected. " +
                  "[features]: ", features)
        else:
            message = self.MsgParser.ParseWelcome(message)
            features = message[0]
            DIMS = message[1]
            print(features, DIMS)
            self.Dimension.emit(DIMS)

    # Slots
    def ConnectTCP(self, Name):
        S = self.Servers.get(Name)
        if (S != None):
            self.TcpSocket.disconnectFromHost()
            self.TcpSocket.connectToHost(S.Address, S.Port)

    def CreateMatch(self, name: str):
        features = self.featuresMatch
        message = self.MsgBuilder.CREATE_MATCH(name, features)
        self.TcpSocket.write(message.encode("ASCII"))
        print(message)

    def CreateMatchFeatures(self, features: list):
        featureString = str()
        for feature in features:
            featureString += feature + ","
        self.featuresMatch = featureString[:-1]
        print(self.featuresMatch)

    def JoinMatch(self, name: str, colors: list):
        self.TimerListMatch.stop()
        colorString = str()
        for color in colors:
            colorString += str(color) + ","
        color = colorString[:-1]
        message = self.MsgBuilder.JOIN_MATCH(name, color)
        self.TcpSocket.write(message.encode("ASCII"))

    def ListGames(self):
        message = self.MsgBuilder.LIST_GAMES()
        self.TcpSocket.write(message.encode("ASCII"))

    def ListMatches(self):
        message = self.MsgBuilder.LIST_MATCHES("Pong")
        self.TcpSocket.write(message.encode("ASCII"))

    def LeavingMatch(self, reason: str):
        message = self.MsgBuilder.LEAVING_MATCH(reason)
        self.TcpSocket.write(message.encode("ASCII"))

    def MatchFeatures(self, name: str):
        message = self.MsgBuilder.MATCH_FEATURES(name)
        self.TcpSocket.write(message.encode("ASCII"))

    def Ready(self):
        message = self.MsgBuilder.I_AM_READY()
        self.TcpSocket.write(message.encode("ASCII"))

    def ReportKey(self, keys: str):
        # print("Match UDP Socket: " + self.MatchUDPAddress)
        # print("Match UDP Port: " + str(self.MatchUDPPort))
        self.SEQ = self.SEQ + 1
        message = self.MsgBuilder.KEYS_PRESSED(self.SEQ, keys).encode("ASCII")
        # print("Message: " + message.decode("ASCII"))
        self.UdpSocket.writeDatagram(
            message, QHostAddress(self.MatchUDPAddress), self.MatchUDPPort)

    def UpdateState(self, State):
        self.State = State
        if (State == int(States.Lobby)):
            self.__Broadcast()
            self.TimerBroadcast.start(BroadcastPeriod)
            self.ListMatches()
            self.TimerListMatch.start(ListMatchPeriod)

        else:
            self.TimerBroadcast.stop()
