import re  # for regex


class MessageBuilder():
    def __AddEnding(self, message: str):
        # append \x00 at the end of the string
        return message + "\x00"

    def HELLO(self, name: str, features: str):
        return self.__AddEnding("HELLO " + name + " BASIC,BEST_OF_3")

    def CREATE_MATCH(self,  name: str, features: str):
        return self.__AddEnding("CREATE_MATCH " + "Pong " + name + " " + features)

    def JOIN_MATCH(self, name: str, color: str):
        return self.__AddEnding("JOIN_MATCH " + name + " " + color)

    def LIST_GAMES(self):
        return self.__AddEnding("LIST_GAMES")

    def LIST_MATCHES(self, game: str):
        return self.__AddEnding("LIST_MATCHES " + game)

    def MATCH_FEATURES(self, name: str):
        return self.__AddEnding("MATCH_FEATURES " + name)

    def LEAVING_MATCH(self, reason: str):
        return self.__AddEnding("LEAVING_MATCH " + reason)

    def I_AM_READY(self):
        return self.__AddEnding("I_AM_READY")

    def KEYS_PRESSED(self, SEQ: int, keys: str):
        return self.__AddEnding(str(SEQ) + " KEYS_PRESSED " + keys)


class MessageChecker():
    # Broadcast
    LOBBY = "LOBBY"

    # Control Protocol
    AVAILABLE_GAMES = "AVAILABLE_GAMES"
    GAMES = "GAMES"
    GAME_ENDED = "GAME_ENDED"
    MATCH = "MATCH"
    MATCH_CREATED = "MATCH_CREATED"
    MATCH_JOINED = "MATCH_JOINED"
    MATCH_STARTED = "MATCH_STARTED"
    WELCOME = "WELCOME"

    SCORE_UPDATE = "SCORE_UPDATE"
    ROUND_UPDATE = "ROUND_UPDATE"
    UPDATE_BALL = "UPDATE_BALL"
    UPDATE_PLAYER = "UPDATE_PLAYER"
    # Error messages
    ERR_CMD_NOT_UNDERSTOOD = "ERR_CMD_NOT_UNDERSTOOD"
    ERR_FAILED_TO_CREATE = "ERR_FAILED_TO_CREATE"
    ERR_FAILED_TO_JOIN = "ERR_FAILED_TO_JOIN"
    ERR_GAME_NOT_EXIST = "ERR_GAME_NOT_EXIST"
    DISCONNECTING_YOU = "DISCONNECTING_YOU"

    # For internal use only
    INVALID = "INVALID"

    def CheckType(self, message: str):
        # check length of the message
        if len(message) == 0:
            return self.INVALID

        # check the command itself
        message = message.split(" ")
        command = message[0]
        if len(message) > 1:
            command1 = message[1]

        # Error Messages
        if command.startswith(self.ERR_CMD_NOT_UNDERSTOOD):
            if len(message) == 1:
                return self.ERR_CMD_NOT_UNDERSTOOD
            return self.INVALID
        if command.startswith(self.ERR_FAILED_TO_CREATE):
            if len(message) > 1:
                return self.ERR_FAILED_TO_CREATE
            return self.INVALID
        if command.startswith(self.ERR_FAILED_TO_JOIN):
            if len(message) > 1:
                return self.ERR_FAILED_TO_JOIN
            return self.INVALID
        if command.startswith(self.ERR_GAME_NOT_EXIST):
            if len(message) == 2:
                return self.ERR_GAME_NOT_EXIST
            return self.INVALID
        if command.startswith(self.DISCONNECTING_YOU):
            if len(message) > 1:
                return self.DISCONNECTING_YOU
            return self.INVALID

        if command.startswith(self.LOBBY):
            if len(message) == 2:
                return self.LOBBY
            return self.INVALID

        if command.startswith(self.AVAILABLE_GAMES):
            if len(message) == 2:
                return self.AVAILABLE_GAMES
            return self.INVALID
        if command == "MATCH":
            if len(message) > 1:
                return self.MATCH
            return self.INVALID
        if command.startswith(self.GAMES):
            if len(message) >= 2 and len(message) <= 3:
                return self.GAMES
            return "Wait for matches"
        if command.startswith(self.GAME_ENDED):
            return self.GAME_ENDED
        if command.startswith(self.MATCH_CREATED):
            if len(message) == 1:
                return self.MATCH_CREATED
            return self.INVALID
        if command.startswith(self.MATCH_JOINED):
            if len(message) == 2:
                return self.MATCH_JOINED
            return self.INVALID
        if command.startswith(self.MATCH_STARTED):
            if len(message) == 5:
                return self.MATCH_STARTED
            return self.INVALID
        if command.startswith(self.WELCOME):
            if len(message) == 2:
                return self.WELCOME
            return self.INVALID

        if command.startswith(self.SCORE_UPDATE):
            if len(message) == 3:
                return self.SCORE_UPDATE
            return self.INVALID

        if command.startswith(self.ROUND_UPDATE):
            if len(message) == 2:
                return self.ROUND_UPDATE
            return self.INVALID

        if command1.startswith(self.UPDATE_BALL):
            if len(message) >= 7:
                return self.UPDATE_BALL
            return self.INVALID
        if command1.startswith(self.UPDATE_PLAYER):
            if len(message) >= 7:
                return self.UPDATE_PLAYER
            return self.INVALID


class MessageParser():

    def ParseScoreUpdate(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        player = message[1]
        score = message[2]
        return player, score

    def ParseRoundUpdate(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        Round = message[1]
        return Round

    def ParseUpdateBall(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        ball_no = message[2]
        x = message[3]
        y = message[4]
        x_v = message[5]
        y_v = message[6]
        return ball_no, x, y, x_v, y_v

    def ParseUpdatePlayer(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        player = message[2]
        x = message[3]
        y = message[4]
        x_v = message[5]
        y_v = message[6]
        return player, x, y, x_v, y_v

    def ParseAvailableGames(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        games = message[1]
        games = games.split(",")
        return games

    def ParseGames(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        game = message[1]
        matches = list()
        if (len(message) == 3):
            matches = message[2]
            matches = matches.split(",")
        return game, matches

    def ParseMatch(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        game = message[1]
        name = message[2]
        features = ["No feature"]
        if (len(message) == 4):
            features = message[3]
            features = features.split(",")
            features = set(features)
            featureString = str()
            for feature in features:
                featureString += feature + ","
            featureString = featureString[:-1]
            features = featureString.split(",")
        return game, name, features

    def ParseMatchJoined(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        playerID = message[1]
        return playerID

    def ParseMatchStarted(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        port = message[1]
        player1 = message[2]
        list1 = message[3]
        player2 = list1[len(list1)-1]
        color1 = list1[:-2]
        color2 = message[4]
        colorlist = list()
        colorlist = [player1, color1, player2, color2]
        return port, colorlist

    def ParseWelcome(self, message: str):
        message = self.__Remove_x00(message)
        if re.search("DIMS", message) == None:
            message = message.split(" ")
            features = message[1]
            features = features.split(",")
            return features
        else:
            message = message.split(" ")
            data = message[1].split(",")
            count = 0
            for i in range(0, len(data)-1):
                if data[i] == "DIMS":
                    break
                count = count + 1
            featureString = str()
            for i in range(0, count):
                featureString += data[i] + ","
            for i in range(count + 5, len(data)):
                featureString += data[i] + ","
            featureString = featureString[:-1]
            features = featureString.split(",")
            DIMS = str()
            for i in range(count + 1, count + 5):
                DIMS += data[i] + ","
            DIMS = DIMS[:-1]
            DIMS = DIMS.split(",")
            return features, DIMS

    def __Remove_x00(self, message: str):
        message = message.replace('\x00', '')
        return message
