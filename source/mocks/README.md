# Mocks
This folder provides the mock classes to be used while developing features independently.
This should be replaced by unit testing in the end.

# Dependencies
Refer to [the project's README.md](../../README.md) for the list of dependencies.

# Available mocks
- [p19_mock_client.py](p19_mock_client.py) fakes the behavior of a client that
  - broadcasts its availability using a UDP socket.

# Running mocks
```
python3 name_of_mock.py
```
