from PyQt5.QtCore import QCoreApplication, QObject, QTimer
from PyQt5.QtNetwork import QHostAddress, QTcpSocket, QUdpSocket

import random


class MockClient(QObject):
    def __init__(self, parent=None):
        super(MockClient, self).__init__(parent)
        self.broadcaster = QUdpSocket(self)
        self.broadcaster.error.connect(self.__on_BroadcasterError)
        self.broadcaster.readyRead.connect(self.__on_BroadcasterReadyRead)

        datagram = "DISCOVER_LOBBY\x00".encode("ASCII")
        self.broadcaster.writeDatagram(
            datagram, QHostAddress.Broadcast, 54000)

        self.matchSocket = QUdpSocket(self)
        self.matchSocket.readyRead.connect(self.__on_MatchSocket_ReadyRead)
        self.matchSocket.bind(random.randrange(40000, 49000))
        self.matchAddress = ""
        self.matchPort = 0
        # This timer is necessary for the cancellation of the process inside the terminal
        # Read the explanation here
        # https://machinekoder.com/how-to-not-shoot-yourself-in-the-foot-using-python-qt/
        self.m_Timer = QTimer()
        self.m_Timer.timeout.connect(lambda: None)
        self.m_Timer.start(100)

    def __on_BroadcasterError(self, err):
        # TODO: print the error messages as strings
        # More infos at https://doc.qt.io/qt-5/qabstractsocket.html#SocketError-enum
        print("Broadcast Sender error: " + str(err))
        exit(1)

    def __on_BroadcasterReadyRead(self):
        while self.broadcaster.hasPendingDatagrams():
            receivedDatagram = self.broadcaster.receiveDatagram()
            addr = receivedDatagram.senderAddress()
            port = receivedDatagram.senderPort()
            data = receivedDatagram.data()
            print("New UDP signal from "
                  + addr.toString()
                  + " "
                  + str(port)
                  + ": "
                  + str(data))

        tcpsoc = QTcpSocket(self)
        tcpsoc.connected.connect(self.__SendHello)
        tcpsoc.disconnected.connect(self.__on_Disconnected)
        tcpsoc.readyRead.connect(self.__Read)

        tcpsoc.connectToHost(addr, 54001)

    def __on_MatchSocket_ReadyRead(self):
        message, address, port = self.matchSocket.readDatagram(1400)
        print(message)

    def __Read(self):
        sock = self.sender()
        message = str(sock.readAll(), encoding="ASCII")
        message = message.replace('\x00', '')
        print("[Lobby] " + message)

        if message.startswith("WELCOME"):
            sock.write("LIST_GAMES\x00".encode("ASCII"))

        if message.startswith("AVAILABLE_GAMES"):
            sock.write("LIST_MATCHES Pong\x00".encode("ASCII"))

        if message.startswith("GAMES"):
            matchlist = message.split(" ")
            # discard the first 2 elements of the message: "GAMES" and "Pong"
            matchlist = matchlist[2:]
            if len(matchlist) == 0:
                print("No matches found")
                sock.write(
                    "CREATE_MATCH Pong testmatch BASIC\x00".encode("ASCII"))
            else:
                print(matchlist)
                sock.write(
                    ("JOIN_MATCH "+matchlist[0]+" 123,115,100\x00").encode("ASCII"))

        if message.startswith("MATCH_CREATED"):
            sock.write("JOIN_MATCH testmatch 255,255,255\x00".encode("ASCII"))

        if message.startswith("MATCH_STARTED"):
            sock.write("I_AM_READY\x00".encode("ASCII"))
            message = message.split(" ")
            self.matchPort = int(message[1])
            self.matchAddress = sock.peerAddress().toString()
            self.matchAddress = self.matchAddress.split("::ffff:")[1]

            self.matchSocket.writeDatagram(
                "0 KEYS_PRESSED SPACE\x00".encode("ASCII"),
                QHostAddress(self.matchAddress),  # TODO: do this dynamically
                self.matchPort)
        # TODO: add the rest of the messages

    def __SendHello(self):
        sock = self.sender()
        sock.write("HELLO MOCKNAME BASIC\x00".encode("ASCII"))

    def __on_Disconnected(self):
        print("Disconnected from server.")
        exit(1)


if __name__ == '__main__':
    import sys
    app = QCoreApplication(sys.argv)
    server = MockClient()
    sys.exit(app.exec_())
