# Server
## Purpose
This is the server side code for the game.

## Development
- A [mock client](../mocks/p19_mock_client.py) is provided, which
  - sends the broadcast to port 54000 of any available machine

## Execution
```
python3 p19_server.py
```
