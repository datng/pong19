# Server TODOS:
## Communication:
### Broadcast:
- [x] DISCOVER_LOBBY
- [x] LOBBY

### Control Protocol:
- [x] HELLO
- [x] WELCOME
- [x] LIST_GAMES
- [x] AVAILABLE_GAMES
- [x] CREATE_MATCH
- [x] MATCH_CREATED
- [x] LIST_MATCHES
- [x] GAMES
- [ ] MATCH_FEATURES
- [ ] MATCH
- [x] JOIN_MATCH
- [x] MATCH_JOINED
- [x] MATCH_STARTED
- [x] GAME_ENDED

### Error Messages:
- [x] ERR_CMD_NOT_UNDERSTOOD
- [x] ERR_FAILED_TO_CREATE
- [x] ERR_FAILED_TO_JOIN
- [ ] ERR_GAME_NOT_EXIST
- [ ] DISCONNECTING_YOU
- [ ] LEAVING_MATCH

### Game Progress
- [ ] SCORE_UPDATE
- [ ] I_AM_READY

### Game Protocol
- [ ] KEYS_PRESSED
- [ ] UPDATE_BALL
- [ ] UPDATE_PLAYER