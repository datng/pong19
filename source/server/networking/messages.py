import re  # for regex


class MessageBuilder():
    def __AddEndingTo(self, message: str):
        # append \x00 at the end of the string
        return message + "\x00"

    def AvailableGames(self, games: str):
        return self.__AddEndingTo("AVAILABLE_GAMES " + games)

    def ErrCmdNotUnderstood(self):
        return self.__AddEndingTo("ERR_CMD_NOT_UNDERSTOOD")

    def ErrFailedToCreate(self, reason: str):
        return self.__AddEndingTo("ERR_FAILED_TO_CREATE " + reason)

    def ErrFailedToJoin(self, reason: str):
        return self.__AddEndingTo("ERR_FAILED_TO_JOIN " + reason)

    def ErrGameNotExist(self, matchname: str):
        return self.__AddEndingTo("ERR_GAME_NOT_EXIST " + matchname)

    # matches are a list of tuples, each of which is a socket and a name
    def Games(self, matches: list):
        matchliststring = str()
        for match in matches:
            matchliststring += match + ","
        matchliststring = matchliststring[:-1]
        message = "GAMES Pong"
        if len(matchliststring) > 0:
            message += " " + matchliststring
        return self.__AddEndingTo(message)

    def GameEnded(self, reason: str):
        return self.__AddEndingTo("GAME_ENDED " + reason)

    def Lobby(self, port: int):
        return self.__AddEndingTo("LOBBY " + str(port))

    def Match(self, matchName: str, features: set):
        featuresString = ""
        for feature in features:
            featuresString += feature + ","
        if len(featuresString) > 0:
            featuresString = featuresString[:-1]
        return self.__AddEndingTo("MATCH Pong " + matchName + " " + featuresString)

    def MatchCreated(self):
        return self.__AddEndingTo("MATCH_CREATED")

    def MatchJoined(self, playerID: int):
        return self.__AddEndingTo("MATCH_JOINED " + str(playerID))

    # Example: MATCH_STARTED 45000 1 117,112,179,2 27,158,119
    def MatchStarted(self, port: int, hostColor: str, guestColor: str):
        return self.__AddEndingTo("MATCH_STARTED "
                                  + str(port)
                                  + " 1 "
                                  + hostColor
                                  + ",2 "
                                  + guestColor)

    def ScoreUpdate(self, playerID: int, score: int):
        return self.__AddEndingTo("SCORE_UPDATE" + " " + str(playerID) + " " + str(score))

    # Best of 3 feature.
    def RoundUpdate(self, Round: int):
        return self.__AddEndingTo("ROUND_UPDATE" + " " + str(Round))

    def Welcome(self, features):
        return self.__AddEndingTo("WELCOME " + features)


class MessageChecker():
    # Broadcast
    DISCOVER_LOBBY = "DISCOVER_LOBBY"

    # Control Protocol
    HELLO = "HELLO"
    LIST_GAMES = "LIST_GAMES"
    CREATE_MATCH = "CREATE_MATCH"
    LIST_MATCHES = "LIST_MATCHES"
    MATCH_FEATURES = "MATCH_FEATURES"
    JOIN_MATCH = "JOIN_MATCH"

    # Error Messages
    LEAVING_MATCH = "LEAVING_MATCH"

    # Game progress
    I_AM_READY = "I_AM_READY"

    # For internal use only
    INVALID = "INVALID"

    def CheckType(self, message: str):
        # check length of the message
        if len(message) == 0:
            return self.INVALID

        # check the command itself
        message = message.split(" ")
        command = message[0]

        if command.startswith(self.DISCOVER_LOBBY):
            if len(message) == 1:
                return self.DISCOVER_LOBBY
            return self.INVALID

        if command.startswith(self.HELLO):
            if len(message) == 3:
                return self.HELLO
            return self.INVALID

        if command.startswith(self.LIST_GAMES):
            if len(message) == 1:
                return self.LIST_GAMES
            return self.INVALID

        if command.startswith(self.CREATE_MATCH):
            if len(message) == 4:
                if "," in message[2]:
                    return self.INVALID
                return self.CREATE_MATCH
            return self.INVALID

        if command.startswith(self.LIST_MATCHES):
            if len(message) == 2:
                return self.LIST_MATCHES
            return self.INVALID

        if command.startswith(self.MATCH_FEATURES):
            if len(message) == 2:
                return self.MATCH_FEATURES
            return self.INVALID

        if command.startswith(self.JOIN_MATCH):
            if len(message) == 3:
                return self.JOIN_MATCH
            return self.INVALID

        if command.startswith(self.LEAVING_MATCH):
            # The specified reason can be variably long, so we don't check the word count here
            return self.LEAVING_MATCH

        if command.startswith(self.I_AM_READY):
            if len(message) == 1:
                return self.I_AM_READY
            return self.INVALID
        # if no known command is found
        return self.INVALID


class MessageParser():
    Invalid: str = "INVALID"

    def ParseCreateMatch(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        gameType = message[1]
        matchName = message[2]
        matchFeatures = message[3]
        matchFeatures = matchFeatures.split(",")
        matchFeatures = set(matchFeatures)
        return gameType, matchName, matchFeatures

    def ParseHello(self, message: str):
        # HELLO [NAME] [list<ClientFeatures]
        message = self.__Remove_x00(message)
        message = message.split(" ")
        name = message[1]
        features = message[2]
        features = features.split(",")
        features = set(features)
        return name, features

    def ParseJoinMatch(self, message: str):
        message = self.__Remove_x00(message)
        message = message.split(" ")
        matchName = message[1]
        color = message[2]
        return matchName, color

    def ParseListMatches(self, message: str):
        # LIST_MATCHES [GAME]
        message = self.__Remove_x00(message)
        message = message.split(" ")
        if len(message) != 2:
            return self.Invalid

        gamename = message[1]
        return gamename

    def ParseLeavingMatch(self, message: str):
        message = self.__Remove_x00(message)
        message = message[14:]
        return message

    def ParseMatchFeatures(self, message: str):
        # MATCH_FEATURES [NAME]
        message = self.__Remove_x00(message)
        message = message.split(" ")
        if len(message) != 2:
            return self.Invalid
        matchname = message[1]
        return matchname

    def __Remove_x00(self, message: str):
        message = message.replace('\x00', '')
        return message
