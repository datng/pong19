from PyQt5.QtCore import pyqtSignal, QObject
from PyQt5.QtNetwork import QAbstractSocket, QHostAddress, QTcpServer, QTcpSocket, QUdpSocket

from .messages import MessageBuilder, MessageChecker, MessageParser

# Module name to put in the log. Logger can be made later but totally not neccessary. TODO
NETWORKING = "\033[95m[Networking]  \033[0m"


class MatchSocket(QObject):
    def __init__(self, port: int, parent=None):
        super(MatchSocket, self).__init__(parent)
        self.Socket = QUdpSocket(self)
        self.Socket.bind(port)
        self.IsBusy = False


class p19s_Networking(QObject):
    '''
    The p19Server_Networking class contains the logic regarding networking inside the server

    Attributes:
        NewConnection: The signal that is emitted when a new connection is detected.

    '''
    ###############
    # Lobby signals
    ###############
    NewConnection = pyqtSignal(QTcpSocket)
    UpdateClient = pyqtSignal(QTcpSocket,  # client socket
                              str,  # client name
                              set)  # client features

    # client that is being removed, reason for removal
    RemoveClient = pyqtSignal(QTcpSocket, str)

    CreateMatchReceived = pyqtSignal(QTcpSocket,  # socket of client who created the match
                                     str,  # game type
                                     str,  # match name
                                     set)  # match features

    # udp socket and list of parsed KEYS_PRESSED sequences
    KeysPressedReceived = pyqtSignal(QUdpSocket, list)

    JoinMatchReceived = pyqtSignal(QTcpSocket,  # socket of the client who joins th match
                                   str,  # match name
                                   str)  # client color

    ListMatchesReceived = pyqtSignal(QTcpSocket)

    LeavingMatchReceived = pyqtSignal(QTcpSocket, str)

    # socket to send features back, match name
    MatchFeaturesReceived = pyqtSignal(QTcpSocket, str)

    PlayerReady = pyqtSignal(QTcpSocket)

    # listener socket, associated host socket for the match
    UseListener = pyqtSignal(QUdpSocket, QTcpSocket)

    def __init__(self, parent=None):
        ''' The default constructor '''
        super(p19s_Networking, self).__init__(parent)

        # Message checker and builder
        self.MsgBuilder = MessageBuilder()
        self.MsgChecker = MessageChecker()
        self.MsgParser = MessageParser()

        # The socket that receives the client broadcasts.
        self.BroadcastReceiver = QUdpSocket(self)
        self.BroadcastReceiver.error.connect(self.__on_BroadcastReceiverError)
        self.BroadcastReceiver.readyRead.connect(self.__ReadBroadcast)
        self.BroadcastReceiver.bind(54000)

        # The TCP server for lobby requests
        self.LobbyServer = QTcpServer(self)
        self.LobbyServer.acceptError.connect(self.__on_LobbyServerAcceptError)
        self.LobbyServer.newConnection.connect(
            self.__on_LobbyServerNewConnection)
        self.LobbyServer.listen(QHostAddress.Any, 54001)

        # A list of udp sockets and their availablilities
        self.MatchSocketList = list()  # [[socket, isbusy], ...]

        for i in range(54010, 54100):
            self.MatchSocketList.append(MatchSocket(i, self))

    def MatchCreated(self, socket: QTcpSocket, name: str):
        self.SendMatchCreated(socket)

    def ReleaseListener(self, socket: QUdpSocket):
        if socket != None:
            print(NETWORKING, "Releasing listener at port " +
                  str(socket.localPort()))
            for listener in self.MatchSocketList:
                if listener.Socket == socket:
                    listener.IsBusy = False

    def SendErrCmdNotUnderstood(self, socket: QTcpSocket):
        response = self.MsgBuilder.ErrCmdNotUnderstood()
        socket.write(response.encode("ASCII"))

    def SendErrFailedToCreate(self, socket: QTcpSocket, reason: str):
        response = self.MsgBuilder.ErrFailedToCreate(reason)
        socket.write(response.encode("ASCII"))

    def SendErrFailedToJoin(self, socket: QTcpSocket, reason: str):
        response = self.MsgBuilder.ErrFailedToJoin(reason)
        socket.write(response.encode("ASCII"))

    def SendErrGameNotExist(self, socket: QTcpSocket, matchname: str):
        response = self.MsgBuilder.ErrGameNotExist(matchname)
        socket.write(response.encode("ASCII"))

    def SendGameEnded(self, socket: QTcpSocket, reason: str):
        response = self.MsgBuilder.GameEnded(reason)
        socket.write(response.encode("ASCII"))

    def SendGames(self, socket: QTcpSocket, matchlist: str):
        response = self.MsgBuilder.Games(matchlist)
        socket.write(response.encode("ASCII"))

    def SendMatchCreated(self, socket: QTcpSocket):
        response = self.MsgBuilder.MatchCreated()
        socket.write(response.encode("ASCII"))

    def SendMatchFeatures(self, socket: QTcpSocket, matchName: str, features: set):
        response = self.MsgBuilder.Match(matchName, features)
        socket.write(response.encode("ASCII"))

    def SendMatchJoined(self, socket: QTcpSocket, playerID: int):
        response = self.MsgBuilder.MatchJoined(playerID)
        socket.write(response.encode("ASCII"))

    def SendMatchStarted(self, hostSocket: QTcpSocket, hostColor: str, guestSocket: QTcpSocket, guestColor: str):
        # TODO: add the UDP socket of the server manually
        available_port: int

        for listener in self.MatchSocketList:
            if listener.IsBusy == False:
                print(NETWORKING, "Using listener at port "
                      + str(listener.Socket.localPort()))
                available_port = listener.Socket.localPort()
                listener.IsBusy = True  # socket is busy

                self.UseListener.emit(listener.Socket, hostSocket)
                break

        response = self.MsgBuilder.MatchStarted(available_port,
                                                hostColor,
                                                guestColor)
        hostSocket.write(response.encode("ASCII"))
        guestSocket.write(response.encode("ASCII"))

    def SendScoreUpdate(self, hostSocket: QTcpSocket, guestSocket: QTcpSocket, playerID: int, score: int):
        response = self.MsgBuilder.ScoreUpdate(playerID, score)
        hostSocket.write(response.encode("ASCII"))
        guestSocket.write(response.encode("ASCII"))

    def SendRoundUpdate(self, hostSocket: QTcpSocket, guestSocket: QTcpSocket, Round: int):
        response = self.MsgBuilder.RoundUpdate(Round)
        hostSocket.write(response.encode("ASCII"))
        guestSocket.write(response.encode("ASCII"))

    def SendWelcome(self, socket: QTcpSocket):
        response = self.MsgBuilder.Welcome("BASIC,DIMS,1,1,640,480")
        socket.write(response.encode("ASCII"))

    def __HandleHello(self, socket: QTcpSocket, message: str):
        name, features = self.MsgParser.ParseHello(message)

        featureString = str()
        for feature in features:
            featureString += feature + ", "

        featureString = featureString[:-2]

        print(NETWORKING, "HELLO command detected. "
              + "Updating client with name and features...")

        self.UpdateClient.emit(socket, name, features)

    def __HandleIAmReady(self, socket: QTcpSocket):
        print(NETWORKING, "I_AM_READY command detected.")
        self.PlayerReady.emit(socket)

    def __HandleJoinMatch(self, socket: QTcpSocket, message: str):
        print(NETWORKING, "JOIN_MATCH command detected.")
        gameName, color = self.MsgParser.ParseJoinMatch(message)
        self.JoinMatchReceived.emit(socket, gameName, color)

    def __HandleLeavingMatch(self, socket: QTcpSocket, message: str):
        print(NETWORKING, "LEAVING_MATCH command detected.")
        reason = self.MsgParser.ParseLeavingMatch(message)
        self.LeavingMatchReceived.emit(socket, reason)

    def __HandleListGames(self, socket: QTcpSocket):
        print(NETWORKING, "LIST_GAMES command detected.")
        response = self.MsgBuilder.AvailableGames("Pong")
        socket.write(response.encode("ASCII"))

    def __HandleListMatches(self, socket: QTcpSocket, message: str):
        # print(NETWORKING, "LIST_MATCHES command detected.")
        parsed = self.MsgParser.ParseListMatches(message)
        if parsed == MessageParser.Invalid:
            self.SendErrCmdNotUnderstood(socket)
            return

        if parsed == "Pong":
            self.ListMatchesReceived.emit(socket)

    def __HandleMatchFeatures(self, socket: QTcpSocket, message: str):
        print(NETWORKING, "MATCH_FEATURES command detected.")
        matchname = self.MsgParser.ParseMatchFeatures(message)
        self.MatchFeaturesReceived.emit(socket, matchname)

    def __on_BroadcastReceiverError(self, err):
        # TODO: print the error messages as strings
        # More infos at https://doc.qt.io/qt-5/qabstractsocket.html#SocketError-enum
        print("Broadcast Receiver error: " + str(err))
        exit(1)

    def __on_ClientDisconnect(self):
        clientSocket = self.sender()
        clientSocket.disconnect()  # remove all signal slot connections

        print(NETWORKING, "Client disconnected.")
        self.RemoveClient.emit(clientSocket, "Client disconnected")

    def __on_ClientReadyRead(self):
        clientSocket = self.sender()
        if clientSocket.bytesAvailable() <= 1024 * 1024:
            receivedmsg = str(clientSocket.readAll(), encoding="ASCII")
            messagelist = receivedmsg.split("\x00")
            if(len(messagelist) != 1):
                messagelist = messagelist[:-1]
            else:
                return

            for message in messagelist:
                # figure out the type of message
                msgType = self.MsgChecker.CheckType(message)
                # handle the message based on its type
                if msgType == MessageChecker.HELLO:
                    self.__HandleHello(clientSocket, message)
                    return

                if msgType == MessageChecker.I_AM_READY:
                    self.__HandleIAmReady(clientSocket)

                if msgType == MessageChecker.INVALID:
                    self.SendErrCmdNotUnderstood(clientSocket)

                if msgType == MessageChecker.JOIN_MATCH:
                    self.__HandleJoinMatch(clientSocket, message)
                    return

                if msgType == MessageChecker.LEAVING_MATCH:
                    self.__HandleLeavingMatch(clientSocket, message)
                    return

                if msgType == MessageChecker.LIST_GAMES:
                    self.__HandleListGames(clientSocket)
                    return

                if msgType == MessageChecker.LIST_MATCHES:
                    self.__HandleListMatches(clientSocket, message)
                    return

                if msgType == MessageChecker.MATCH_FEATURES:
                    self.__HandleMatchFeatures(clientSocket, message)

                if msgType == MessageChecker.CREATE_MATCH:
                    print(NETWORKING, "CREATE_MATCH command detected.")
                    # get gameType, matchName, matchFeatures
                    t, n, f = self.MsgParser.ParseCreateMatch(message)
                    self.CreateMatchReceived.emit(clientSocket, t, n, f)
                    return

        else:
            # Client is sending super long messages.
            print(NETWORKING, "Client is trying to send very long message. "
                  + "Removing...")
            clientSocket.disconnect()
            self.RemoveClient.emit(clientSocket, "Spam")

    def __on_LobbyServerNewConnection(self):
        print(NETWORKING, "New connection detected.")

        socket = self.LobbyServer.nextPendingConnection()
        socket.disconnected.connect(self.__on_ClientDisconnect)
        socket.readyRead.connect(self.__on_ClientReadyRead)

        self.NewConnection.emit(socket)

    def __on_LobbyServerAcceptError(self):
        pass

    def __ReadBroadcast(self):
        '''
        Reads incoming data from the broadcast receiver.

        This function is called when the readyRead signal is emitted by BroadcastReceiver.
        '''
        while self.BroadcastReceiver.hasPendingDatagrams():
            receivedDatagram = self.BroadcastReceiver.receiveDatagram()
            addr = receivedDatagram.senderAddress()
            port = receivedDatagram.senderPort()
            message = str(receivedDatagram.data(), "ASCII")

            if self.MsgChecker.CheckType(message) != self.MsgChecker.DISCOVER_LOBBY:
                # The received datagram is not a discover command, do not response.
                break
            else:
                responseDatagram = self.MsgBuilder.Lobby(54001).encode("ASCII")

            self.BroadcastReceiver.writeDatagram(responseDatagram, addr, port)
