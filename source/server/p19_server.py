from PyQt5.QtCore import QCoreApplication, QObject, QTimer

from networking import p19s_Networking
from lobby import Lobby


class Server(QObject):
    def __init__(self, parent=None):
        super(Server, self).__init__(parent)
        self.m_Network = p19s_Networking(self)
        self.m_Lobby = Lobby(self)

        # Connect different parts together
        self.m_Network.CreateMatchReceived.connect(self.m_Lobby.CreateMatch)
        self.m_Network.JoinMatchReceived.connect(self.m_Lobby.JoinMatch)
        self.m_Network.LeavingMatchReceived.connect(
            self.m_Lobby.LeavingMatchReceived)
        self.m_Network.ListMatchesReceived.connect(self.m_Lobby.ListMatch)
        self.m_Network.MatchFeaturesReceived.connect(
            self.m_Lobby.GetMatchFeatures)
        self.m_Network.NewConnection.connect(self.m_Lobby.AddClient)
        self.m_Network.PlayerReady.connect(self.m_Lobby.PlayerReady)
        self.m_Network.RemoveClient.connect(self.m_Lobby.RemoveClient)
        self.m_Network.UpdateClient.connect(self.m_Lobby.UpdateClient)
        self.m_Network.UseListener.connect(self.m_Lobby.UseListener)

        self.m_Lobby.ClientUpdated.connect(self.m_Network.SendWelcome)
        self.m_Lobby.ErrFailedToCreate.connect(
            self.m_Network.SendErrFailedToCreate)
        self.m_Lobby.ErrFailedToJoin.connect(
            self.m_Network.SendErrFailedToJoin)
        self.m_Lobby.ErrGameNotExist.connect(
            self.m_Network.SendErrGameNotExist)
        self.m_Lobby.GameEnded.connect(self.m_Network.SendGameEnded)
        self.m_Lobby.MatchCreated.connect(self.m_Network.SendMatchCreated)
        self.m_Lobby.MatchJoined.connect(self.m_Network.SendMatchJoined)
        self.m_Lobby.MatchStarted.connect(self.m_Network.SendMatchStarted)
        self.m_Lobby.ReleaseListener.connect(self.m_Network.ReleaseListener)
        self.m_Lobby.Score_Update.connect(self.m_Network.SendScoreUpdate)
        self.m_Lobby.SendMatchFeatures.connect(
            self.m_Network.SendMatchFeatures)
        self.m_Lobby.SendMatchList.connect(self.m_Network.SendGames)
        self.m_Lobby.UpdateRound.connect(self.m_Network.SendRoundUpdate)

        # This timer is necessary for the cancellation of the process inside the terminal
        # Read the explanation here
        # https://machinekoder.com/how-to-not-shoot-yourself-in-the-foot-using-python-qt/
        self.m_Timer = QTimer()
        self.m_Timer.timeout.connect(lambda: None)
        self.m_Timer.start(100)


if __name__ == '__main__':
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    import sys
    app = QCoreApplication(sys.argv)
    server = Server()
    sys.exit(app.exec_())
