from PyQt5.QtCore import pyqtSignal, QObject, QTimer
from PyQt5.QtNetwork import QHostAddress, QTcpSocket, QUdpSocket

import math
import random
import socket

from .client import Client

BALL_SPEED = 7
StrikerSpeed = 20

MATCH = "\033[93m[Match]       \033[0m"


class MatchDimension():
    def __init__(self):
        self.Total_X = 640
        self.Total_Y = 480
        self.Cell_X = 1
        self.Cell_Y = 1
        self.Ball_Size = 8
        self.Limit_Up = self.Total_Y - self.Ball_Size
        self.Limit_Right = self.Total_X - self.Ball_Size
        self.Striker_Length = 120
        self.Striker_Width = 8
        self.Center = [self.Total_X/2 - self.Ball_Size / 2,
                       self.Total_Y/2 - self.Ball_Size / 2]


DIMENSION = MatchDimension()


def NormalizeVel(x: float, y: float):
    magnitude = x * x + y * y
    if magnitude == 0:
        return [0, 0]
    return [x / magnitude * BALL_SPEED, y / magnitude * BALL_SPEED]


class MatchState(QObject):
    # the one who scores will be emitted
    Goal = pyqtSignal(int)
    # ball number, ball position x, y, ball velocity x, y
    Updated_Ball = pyqtSignal(int, int, int, int, int)
    # which player, player position x, y, player velocity x, y
    Updated_Player = pyqtSignal(int, int, int, int, int)

    Scores = [0, 0]

    def __init__(self, parent=None):
        super(MatchState, self).__init__(parent)
        self.Reset()
        self.period = 1/60.0
        self.Timer = QTimer()
        self.Timer.start(self.period*1000)  # update at 60fps

    def Reset(self):
        self.Pos_Ball = DIMENSION.Center
        self.Pos_Player1 = [0, DIMENSION.Total_Y /
                            2 - DIMENSION.Striker_Length / 2]
        self.Pos_Player2 = [DIMENSION.Limit_Right,
                            DIMENSION.Total_Y / 2 - DIMENSION.Striker_Length / 2]
        if random.random() < 0.5:
            self.Vel_Ball = NormalizeVel(-1, 0)
        else:
            self.Vel_Ball = NormalizeVel(1, 0)
        self.Vel_Player1 = [0, 0]
        self.Vel_Player2 = [0, 0]
        self.Key_1 = ""
        self.Key_2 = ""
        self.Scores = [0, 0]

    def Start(self):
        self.Reset()
        self.Timer.timeout.connect(self.UpdateMatch)
        print(MATCH, "Match started.")

    def Stop(self):
        if self.Timer != None:
            del self.Timer
            self.Timer = QTimer()
        print(MATCH, "Match stopped.")

    def UpdateMatch(self):
        self.__UpdatePlayer1()
        self.__UpdatePlayer2()

        # update ball
        projected = [self.Pos_Ball[0] + self.Vel_Ball[0],
                     self.Pos_Ball[1] + self.Vel_Ball[1]]
        clamped = self.__ClampPos(
            self.Pos_Ball, projected, DIMENSION)

        # if ball touches up or down
        if clamped[1] == DIMENSION.Limit_Up or clamped[1] == 0:
            self.Vel_Ball[1] = -self.Vel_Ball[1]

        # if ball is at either horizontal side
        if clamped[0] == DIMENSION.Striker_Width:
            if clamped[1] < self.Pos_Player1[1]:
                self.Goal.emit(2)
                # reset ball
                self.Vel_Ball = NormalizeVel(-1, 0)
                self.Pos_Ball = DIMENSION.Center
            elif clamped[1] > self.Pos_Player1[1] + DIMENSION.Striker_Length:
                self.Goal.emit(2)
                # reset ball
                self.Vel_Ball = NormalizeVel(-1, 0)
                self.Pos_Ball = DIMENSION.Center
            else:
                # TODO: implement function to reflect based on relative position
                self.Pos_Ball = clamped
                self.Vel_Ball[0] = - self.Vel_Ball[0]
                # calculate relative position of center of ball compared to center of player bar
                diff = (self.Pos_Ball[1] + DIMENSION.Ball_Size / 2) \
                    - (self.Pos_Player1[1] + DIMENSION.Striker_Length / 2)
                # get the relative difference (normalized to 1)
                diff = float(diff) / (DIMENSION.Striker_Length / 2)
                # 60 degree is the maximum angle on each side that the ball can fly away from the player bar
                diff = diff * 60
                # convert to radian
                diff = diff * math.pi / 180
                newvel = NormalizeVel(math.cos(diff), math.sin(diff))
                self.Vel_Ball = newvel

        elif clamped[0] == DIMENSION.Limit_Right - DIMENSION.Striker_Width:
            if clamped[1] < self.Pos_Player2[1]:
                self.Goal.emit(1)
                # reset ball
                self.Vel_Ball = NormalizeVel(1, 0)
                self.Pos_Ball = DIMENSION.Center
            elif clamped[1] > self.Pos_Player2[1] + DIMENSION.Striker_Length:
                self.Goal.emit(1)
                # reset ball
                self.Vel_Ball = NormalizeVel(1, 0)
                self.Pos_Ball = DIMENSION.Center
            else:
                # TODO: implement function to reflect based on relative position
                self.Pos_Ball = clamped
                self.Vel_Ball[0] = - self.Vel_Ball[0]
                # calculate relative position of center of ball compared to center of player bar
                diff = (self.Pos_Ball[1] + DIMENSION.Ball_Size / 2) \
                    - (self.Pos_Player2[1] + DIMENSION.Striker_Length / 2)
                # get the relative difference (normalized to 1)
                diff = diff / (DIMENSION.Striker_Length / 2)
                # 60 degree is the maximum angle on each side that the ball can fly away from the player bar
                diff = diff * 60
                # convert to radian
                diff = diff * math.pi / 180
                newvel = NormalizeVel(-math.cos(diff), math.sin(diff))
                self.Vel_Ball = newvel

        else:
            self.Pos_Ball = clamped

        self.Updated_Ball.emit(
            1,
            self.Pos_Ball[0],
            self.Pos_Ball[1],
            self.Vel_Ball[0] / self.period,
            self.Vel_Ball[1] / self.period)

        self.Updated_Player.emit(
            1,
            self.Pos_Player1[0],
            self.Pos_Player1[1],
            self.Vel_Player1[0] / self.period,
            self.Vel_Player1[1] / self.period)

        self.Updated_Player.emit(
            2,
            self.Pos_Player2[0],
            self.Pos_Player2[1],
            self.Vel_Player2[0] / self.period,
            self.Vel_Player2[1] / self.period)

    # current position, projected position, match dimension
    def __ClampPos(self, currentpos, projectedpos, dims):
        # ball is out on the left side
        ratio = 1
        if projectedpos[0] < dims.Striker_Width:
            divident = float(dims.Striker_Width - projectedpos[0])
            divisor = float(currentpos[0] - dims.Striker_Width)
            if divisor == 0:
                ratio = 1
            else:
                ratio = float(divident / divisor)
        # ball is out on the right side
        if projectedpos[0] > dims.Limit_Right - dims.Striker_Width:
            divident = float(projectedpos[0] -
                             (dims.Limit_Right - dims.Striker_Width))
            divisor = float(dims.Limit_Right -
                            dims.Striker_Width - currentpos[0])
            if divisor == 0:
                ratio = 1
            else:
                ratio = float(divident / divisor)
        # ball is out on the bottom side
        if projectedpos[1] < 0:
            if currentpos[1] == 0:
                ratio = 1
            else:
                ratio = float(- projectedpos[1] / currentpos[1])

        # ball is out on the top side
        if projectedpos[1] > dims.Limit_Up:
            divident = float(projectedpos[1] - dims.Limit_Up)
            divisor = float(dims.Limit_Up - currentpos[1])
            if divisor == 0:
                ratio = 1
            else:
                ratio = float(divident / divisor)

        x = int(currentpos[0] + self.Vel_Ball[0] / ratio)
        y = int(currentpos[1] + self.Vel_Ball[1] / ratio)
        x = max(x, dims.Striker_Width)
        x = min(x, dims.Limit_Right - dims.Striker_Width)
        y = max(y, 0)
        y = min(y, dims.Limit_Up)

        return [x, y]

    def __UpdatePlayer1(self):
        if self.Key_1 == "DOWN":
            if self.Pos_Player1[1] < DIMENSION.Total_Y - DIMENSION.Striker_Length:
                self.Pos_Player1[1] += DIMENSION.Cell_Y * StrikerSpeed
            else:
                self.Pos_Player1[1] = DIMENSION.Total_Y - \
                    DIMENSION.Striker_Length
        elif self.Key_1 == "UP":
            if self.Pos_Player1[1] > 0:
                self.Pos_Player1[1] -= DIMENSION.Cell_Y * StrikerSpeed
            else:
                self.Pos_Player1[1] = 0
        # reset this key
        self.Key_1 = ""

    def __UpdatePlayer2(self):
        if self.Key_2 == "DOWN":
            if self.Pos_Player2[1] < DIMENSION.Total_Y - DIMENSION.Striker_Length:
                self.Pos_Player2[1] += DIMENSION.Cell_Y * StrikerSpeed
            else:
                self.Pos_Player2[1] = DIMENSION.Total_Y - \
                    DIMENSION.Striker_Length
        elif self.Key_2 == "UP":
            if self.Pos_Player2[1] > 0:
                self.Pos_Player2[1] -= DIMENSION.Cell_Y * StrikerSpeed
            else:
                self.Pos_Player2[1] = 0
        # reset this key
        self.Key_2 = ""


class Match(QObject):
    ReleaseListener = pyqtSignal(QUdpSocket)
    # 2 player sockets, playerID of scorer and the new score
    Score_Update = pyqtSignal(QTcpSocket, QTcpSocket, int, int)
    UpdateRound = pyqtSignal(QTcpSocket, QTcpSocket, int)

    # reason
    MatchEnded = pyqtSignal(str)

    def __init__(self, host: Client, name: str, features: set, parent=None):
        super(Match, self).__init__(parent)
        self.Host = host
        self.Guest: Client = None
        self.Name = name
        self.Features = self.__FilterFeatures(features)
        self.InProgress = False
        self.Listener: QUdpSocket = None
        self.Player1 = [QHostAddress.Null, 0]  # address and port of player 1
        self.Player2 = [QHostAddress.Null, 0]  # address and port of player 2
        self.Player1Seq = 0
        self.Player2Seq = 0

        # in case the game features list contains BEST_OF_3,
        # the one that wins 2 rounds first wins.
        self.OverallScores = [0, 0]
        self.Round = 1

        self.State = MatchState()
        self.State.Updated_Ball.connect(self.__on_Status_Updated_Ball)
        self.State.Updated_Player.connect(self.__on_Status_Updated_Player)
        self.State.Goal.connect(self.__on_Goal)

        # this timer ticks until both host and guest is ready to start the state machine
        self.timer = QTimer()

    def AddPlayerSocket(self, address: QHostAddress, port: int):
        # in case the clients are from the same machine
        if(self.Host.Socket.peerAddress().toIPv4Address() == self.Guest.Socket.peerAddress().toIPv4Address()
           and self.Host.Socket.peerAddress().toIPv4Address() == address.toIPv4Address()):
                # assign in a first come first serve manner
            if self.Player1[0] == QHostAddress.Null and self.Player1[1] == 0:
                self.Player1 = [address, port]
                print(MATCH, "Player 1 added to " + self.Name)
            elif self.Player2[0] == QHostAddress.Null and self.Player2[1] == 0:
                if self.Player1[0] != address or self.Player1[1] != port:
                    self.Player2 = [address, port]
                    print(MATCH, "Player 2 added to " + self.Name)
            else:
                print(MATCH, "Attempt was made to add a third player to " + self.Name)
        else:
            if address.toIPv4Address() == self.Host.Socket.peerAddress().toIPv4Address():
                if self.Player1[0] == QHostAddress.Null and self.Player1[1] == 0:
                    self.Player1 = [address, port]
                    print(MATCH, "Player 1 added to " + self.Name)
                else:
                    print(MATCH, self.Name
                          + ": Player 1: An UDP socket from the same IP has been registered.")
            elif address.toIPv4Address() == self.Guest.Socket.peerAddress().toIPv4Address():
                if self.Player2[0] == QHostAddress.Null and self.Player2[1] == 0:
                    self.Player2 = [address, port]
                    print(MATCH, "Player 2 added to " + self.Name)
                else:
                    print(MATCH, self.Name
                          + ": Player 2: An UDP socket from the same IP has been registered.")
            else:
                print(MATCH, self.Name + ": Unknown IP address.")

    def AddGuest(self, client: Client, color: str):
        self.Guest = client
        self.Guest.Color = color

    def ParseKeysPressed(self, message: str):
        # break down the message into a list of commands
        commands: list = list()
        sq = message.split('\x00')
        for command in sq:
            breakdown = command.split(" ")
            if len(breakdown) == 3:
                # check type, this is needed only for this because the way the message is built is different
                if breakdown[1] == "KEYS_PRESSED":
                    keyset = set(breakdown[2].split(","))

                commands.append([breakdown[0], keyset])
        return commands

    def RemoveGuest(self):
        self.Guest = None
        self.InProgress = False
        self.Stop()
        self.MatchEnded.emit("Guest left.")

    def Start(self):
        self.timer.timeout.connect(self.__StartStateMachine)
        # ticks every tenth of a second to try and start the state machine
        self.timer.start(100)

    def Stop(self):
        self.Host.IsReady = False
        self.Guest.IsReady = False
        self.InProgress = False
        self.State.Stop()
        self.Player1 = ["", 0]  # address and port of player 1
        self.Player2 = ["", 0]  # address and port of player 2

    def UpdateHost(self, color: str):
        self.Host.Color = color

    def UseListener(self, socket: QUdpSocket):
        self.Listener = socket
        self.Listener.error.connect(self.__on_ListenerError)
        self.Listener.readyRead.connect(self.__on_ListenerReadyRead)

    def __FilterFeatures(self, features: set):
        filtered = set()
        filtered.add("BASIC")
        if "BEST_OF_3" in features:
            filtered.add("BEST_OF_3")
        return filtered

    def __on_Goal(self, playerID: int):
        # Delay after scores.
        # self.State.Timer.disconnect()
        # QTimer.singleShot(500, lambda : self.State.Timer.timeout.connect(self.State.UpdateMatch))

        index = playerID - 1
        self.State.Scores[index] += 1
        self.Score_Update.emit(
            self.Host.Socket,
            self.Guest.Socket,
            playerID,
            self.State.Scores[index])
        if self.State.Scores[index] == 10:
            finished = False
            if "BEST_OF_3" in self.Features:
                self.State.Reset()
                self.OverallScores[index] += 1
                self.Round += 1
                self.UpdateRound.emit(
                    self.Host.Socket,
                    self.Guest.Socket,
                    self.Round)

                self.Score_Update.emit(
                    self.Host.Socket,
                    self.Guest.Socket,
                    1,
                    0)
                self.Score_Update.emit(
                    self.Host.Socket,
                    self.Guest.Socket,
                    2,
                    0)
                if self.OverallScores[index] == 2:
                    finished = True
            else:
                finished = True

            if finished == True:
                self.Stop()
                self.MatchEnded.emit("Player " + str(playerID) + " won.")

    def __on_ListenerError(self, err):
        socket: QUdpSocket = self.sender()
        print(MATCH, "Match socket at "
              + str(socket.localPort)
              + " encountered an error. Status code:"
              + str(err))

    def __on_ListenerReadyRead(self):
        socket = self.sender()
        if socket.pendingDatagramSize() <= 1400:
            message, addr, port = socket.readDatagram(1400)
            if (self.Player1[0] == QHostAddress.Null and self.Player1[1] == 0) or (self.Player2[0] == QHostAddress.Null and self.Player2[1] == 0):
                addr = QHostAddress(addr.toIPv4Address())
                self.AddPlayerSocket(addr, port)

            message = message.decode("ASCII")
            message = self.ParseKeysPressed(message)

            origin = 0
            if addr.toIPv4Address() == self.Player1[0].toIPv4Address() and port == self.Player1[1]:
                origin = 1

            elif addr.toIPv4Address() == self.Player2[0].toIPv4Address() and port == self.Player2[1]:
                origin = 2
            else:
                return

            if message != None:
                # TODO add funciton to register the key events until the end, not stopping at the first
                if "UP" in message[0][1]:
                    if "DOWN" in message[0][1]:
                        if origin == 1:
                            self.State.Key_1 = ""
                        elif origin == 2:
                            self.State.Key_2 = ""
                    else:
                        if origin == 1:
                            self.State.Key_1 = "UP"
                        elif origin == 2:
                            self.State.Key_2 = "UP"
                elif "DOWN" in message[0][1]:
                    if origin == 1:
                        self.State.Key_1 = "DOWN"
                    elif origin == 2:
                        self.State.Key_2 = "DOWN"
            else:
                return
        else:
            socket.readAll()

    def __on_Status_Updated_Ball(self, ballnr: int, posx: int, posy: int, velx: int, vely: int):
        if self.Player1[0] != QHostAddress.Null and self.Player1[1] != 0:
            message = str(str(self.Player1Seq)
                          + " UPDATE_BALL "
                          + str(ballnr) + " "
                          + str(posx) + " "
                          + str(posy) + " "
                          + str(velx) + " "
                          + str(vely) + "\x00")
            self.Listener.writeDatagram(message.encode("ASCII"),
                                        self.Player1[0],
                                        self.Player1[1])
            self.Player1Seq += 1

        if self.Player2[0] != "" and self.Player2[1] != 0:
            message = str(str(self.Player2Seq)
                          + " UPDATE_BALL "
                          + str(ballnr) + " "
                          + str(posx) + " "
                          + str(posy) + " "
                          + str(velx) + " "
                          + str(vely) + "\x00")
            self.Listener.writeDatagram(message.encode("ASCII"),
                                        self.Player2[0],
                                        self.Player2[1])
            self.Player2Seq += 1

    def __on_Status_Updated_Player(self, playerID: int, posx: int, posy: int, velx: int, vely: int):
        if self.Player1[0] != QHostAddress.Null and self.Player1[1] != 0:
            message = str(str(self.Player1Seq)
                          + " UPDATE_PLAYER "
                          + str(playerID) + " "
                          + str(posx) + " "
                          + str(posy) + " "
                          + str(velx) + " "
                          + str(vely) + "\x00")
            self.Listener.writeDatagram(message.encode("ASCII"),
                                        self.Player1[0],
                                        self.Player1[1])
            self.Player1Seq += 1

        if self.Player2[0] != "" and self.Player2[1] != 0:
            message = str(str(self.Player1Seq)
                          + " UPDATE_PLAYER "
                          + str(playerID) + " "
                          + str(posx) + " "
                          + str(posy) + " "
                          + str(velx) + " "
                          + str(vely) + "\x00")
            self.Listener.writeDatagram(message.encode("ASCII"),
                                        self.Player2[0],
                                        self.Player2[1])
            self.Player2Seq += 1

    def __StartStateMachine(self):
        if self.Host.IsReady and self.Guest.IsReady:
            if "BEST_OF_3" in self.Features:
                self.UpdateRound.emit(
                    self.Host.Socket,
                    self.Guest.Socket,
                    self.Round)
            self.InProgress = True
            self.State.Start()
            self.timer.disconnect()
