from PyQt5.QtCore import QObject, pyqtSignal, QTimer
from PyQt5.QtNetwork import QTcpSocket, QUdpSocket

from .client import Client
from .match import Match

# Module name to put in the log. Logger can be made later but totally not neccessary. TODO
LOBBY = "\033[94m[Lobby]       \033[0m"


class Lobby(QObject):
    '''
    The Lobby class describes an abstraction of the lobby.
    '''
    ClientAdded = pyqtSignal(QTcpSocket)
    ClientUpdated = pyqtSignal(QTcpSocket)
    ClientRemoved = pyqtSignal(QTcpSocket)

    # socket of client who wants to join and match name
    ErrFailedToCreate = pyqtSignal(QTcpSocket, str)
    ErrFailedToJoin = pyqtSignal(QTcpSocket, str)
    ErrGameNotExist = pyqtSignal(QTcpSocket, str)

    # 2 player sockets, playerID of scorer and the new score
    Score_Update = pyqtSignal(QTcpSocket, QTcpSocket, int, int)
    UpdateRound = pyqtSignal(QTcpSocket, QTcpSocket, int)

    # player socket and the reason
    GameEnded = pyqtSignal(QTcpSocket, str)

    # the str is for the networking script to keep a list of match names
    MatchCreated = pyqtSignal(QTcpSocket, str)

    # player socket and player ID
    MatchJoined = pyqtSignal(QTcpSocket, int)

    # host socket, host color, guest socket, guest color
    MatchStarted = pyqtSignal(QTcpSocket, str, QTcpSocket, str)

    # socket to send the message back, the match name and the set of features
    SendMatchFeatures = pyqtSignal(QTcpSocket, str, set)

    # socket to send the list to, and the list itself
    SendMatchList = pyqtSignal(QTcpSocket, list)

    ReleaseListener = pyqtSignal(QUdpSocket)

    def __init__(self, parent=None):
        super(Lobby, self).__init__(parent)
        self.Clients = set()
        self.Matches = list()

    def AddClient(self, socket: QTcpSocket, name: str = "", features: set = set()):
        if len(self.Clients) != 0:
            # if clients already exist
            for client in self.Clients:
                if client.Socket.peerAddress() == socket.peerAddress():
                    if client.Socket.peerPort() == socket.peerPort():
                        self.UpdateClient(socket, name, features)
                        return

                    # # region security, stability - comment this region to enable multiple clients per machine
                    # # Same address, different port => delete old client and add new one
                    # client.Socket.disconnect()  # remove all signal slot connections
                    # print("[Lobby] Multiple clients from the same address. "
                    #       + "Removing old client...")
                    # self.RemoveClient(client.Socket, "Multiple clients from same machine.")
                    # print("[Lobby] Adding new client...")
                    # self.Clients.add(Client(socket, name, features))
                    # print("[Lobby] Client added: Address: "
                    #       + socket.peerAddress().toString()
                    #       + ", Port: "
                    #       + str(socket.peerPort()))

                    # self.__PrintClientList()
                    # self.ClientUpdated.emit(socket)
                    # return
                    # # endregion

        self.Clients.add(Client(socket, name, features))
        print(LOBBY, "Client added: Address: "
              + socket.peerAddress().toString()
              + ", Port: "
              + str(socket.peerPort()))
        self.__PrintClientList()
        self.ClientAdded.emit(socket)
        return

    def CreateMatch(self, socket: QTcpSocket, gameType: str, matchName: str, matchFeatures: set):
        for match in self.Matches:
            if match.Name == matchName:
                self.ErrFailedToCreate.emit(socket, "Name already taken")
                return

        featureStr = str()
        for feature in matchFeatures:
            featureStr += feature + ", "
        featureStr = featureStr[:-2]

        print(LOBBY, "Creating match: "
              + gameType + " "
              + matchName + " "
              + featureStr + "...")

        for client in self.Clients:
            if client.Socket == socket:
                match = Match(client, matchName, matchFeatures)
                match.Host.Color = "255,255,255"
                match.MatchEnded.connect(self.__on_MatchEnded)
                match.ReleaseListener.connect(self.ReleaseListener)
                match.Score_Update.connect(self.Score_Update)
                match.UpdateRound.connect(self.UpdateRound)

                self.Matches.append(match)
                break

        self.__PrintMatchList()
        self.MatchCreated.emit(socket, matchName)

    def GetMatchFeatures(self, socket: QTcpSocket, matchname: str):
        for match in self.Matches:
            if match.Name == matchname:
                self.SendMatchFeatures.emit(socket, matchname, match.Features)
                return

    def JoinMatch(self, socket: QTcpSocket, matchname, color: str):
        matchExists = False
        for match in self.Matches:
            if match.Name == matchname:
                matchExists = True
                break
        if matchExists == False:
            self.ErrGameNotExist.emit(socket, matchname)
            return

        for match in self.Matches:
            if match.Name == matchname:
                if socket == match.Host.Socket:
                    if match.InProgress:
                        break
                    match.UpdateHost(color)
                    self.MatchJoined.emit(match.Host.Socket, 1)
                    return

                found = False
                for client in self.Clients:
                    if client.Socket == socket:
                        found = True
                        if match.InProgress:
                            break
                        if(client.Features < match.Features):
                            self.ErrFailedToJoin.emit(
                                client.Socket, "Feature mismatch.")
                            return
                        match.AddGuest(client, color)
                        self.MatchJoined.emit(match.Guest.Socket, 2)
                        match.Start()

                        # wait 100ms before emitting the match started signal
                        timer = QTimer()
                        timer.singleShot(100, lambda:
                                         self.MatchStarted.emit(
                                             match.Host.Socket,
                                             match.Host.Color,
                                             match.Guest.Socket,
                                             match.Guest.Color)
                                         )
                        del timer
                        return

                # can't find the socket in client list
                if found == False:
                    self.ErrFailedToJoin.emit(
                        socket, "You are not in the client list")
        # cannot find match
        self.ErrFailedToJoin.emit(socket, "Match does not exist")
        return

    def ListMatch(self, socket: QTcpSocket):
        matchlist = list()
        for match in self.Matches:
            if match.Guest == None:
                matchlist.append(match.Name)
        self.SendMatchList.emit(socket, matchlist)

    def LeavingMatchReceived(self, socket: QTcpSocket, reason: str):
        # don't remove the client, just end the match
        for match in self.Matches:
            if match.Host.Socket == socket:
                if match.Guest != None:
                    self.GameEnded.emit(match.Guest.Socket,
                                        "Host left. Reason: " + reason)
                self.__RemoveMatch(match)
                break

            if match.Guest != None:
                if match.Guest.Socket == socket:
                    self.GameEnded.emit(match.Host.Socket,
                                        "Guest left. Reason: " + reason)
                self.__RemoveMatch(match)
                break

    def PlayerReady(self, socket: QTcpSocket):
        for match in self.Matches:
            # the I am ready command is valid only for matches that are joined
            if match.Guest != None:
                if socket == match.Host.Socket:
                    match.Host.IsReady = True
                    print(LOBBY, match.Name + ": Host is ready.")
                    return
                if socket == match.Guest.Socket:
                    match.Guest.IsReady = True
                    print(LOBBY, match.Name + ": Guest is ready.")
                    return

    def RemoveClient(self, socket: QTcpSocket, reason: str):
        for client in self.Clients:
            if client.Socket == socket:
                for match in self.Matches:
                    if match.Host.Socket == socket:
                        if match.Guest != None:
                            self.GameEnded.emit(
                                match.Guest.Socket,
                                "Host removed. Reason: " + reason)
                        self.__RemoveMatch(match)
                        break
                    if match.Guest != None:
                        if match.Guest.Socket == socket:
                            self.GameEnded.emit(
                                match.Host.Socket,
                                "Guest removed. Reason: " + reason)
                            self.__RemoveMatch(match)
                            break
                del client.Socket
                self.Clients.remove(client)

                print(LOBBY,
                      "Client removed. Number of current clients: "
                      + str(len(self.Clients)))
                self.ClientRemoved.emit(socket)
                return

    def UpdateClient(self, socket, name: str = "", features: set = set()):
        for client in self.Clients:
            if client.Socket.peerAddress() == socket.peerAddress():
                if client.Socket.peerPort() == socket.peerPort():
                    featureString = str()
                    for feature in features:
                        featureString += feature + ", "
                    featureString = featureString[:-2]

                    print(LOBBY, "Client updated: Address: "
                          + socket.peerAddress().toString()
                          + ", Port : "
                          + str(socket.peerPort())
                          + ", Name: "
                          + name
                          + ", Features: "
                          + featureString)
                    # Same address, same port => update name and features
                    client.Name = name
                    client.Features = features
                    self.__PrintClientList()
                    self.ClientUpdated.emit(socket)
                    return

    def UseListener(self, listener: QUdpSocket, host: QTcpSocket):
        for match in self.Matches:
            if match.Host.Socket == host:
                match.UseListener(listener)
                return

        # if no match is found with that host, release listener
        self.ReleaseListener.emit(listener)

    def __on_MatchEnded(self, reason: str):
        match: Match = self.sender()
        if match.Host.Socket.state() == QTcpSocket.ConnectedState:
            self.GameEnded.emit(match.Host.Socket, reason)
        if match.Guest.Socket.state() == QTcpSocket.ConnectedState:
            self.GameEnded.emit(match.Guest.Socket, reason)
        self.__RemoveMatch(match)

    def __RemoveMatch(self, match: Match):
        print(LOBBY, "Removing match \"" + match.Name + "\"...")
        match.Stop()
        match.ReleaseListener.emit(match.Listener)
        if match.Listener != None:
            match.Listener.disconnect()
            match.Listener = None

        self.Matches.remove(match)
        del match

    def __PrintClientList(self):
        clientList = str()
        for client in self.Clients:
            clientList += "\""+client.Name + "\"" + ", "
        clientList = clientList[:-2]
        print(LOBBY, "Client list: " + clientList)

    def __PrintMatchList(self):
        matchList = str()
        for match in self.Matches:
            matchList += match.Name + ", "
        matchList = matchList[:-2]
        print(LOBBY, "Current matches: " + matchList)
