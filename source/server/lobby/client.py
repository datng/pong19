from PyQt5.QtNetwork import QTcpSocket
from enum import Enum


class Client():
    '''
    The Client class defines an abstraction of a client.

    Attributes:
        Socket: The QTcpSocket used for connection.
        Name: The client's chosen name. Default to "".
        Features: A set of available features on the client.
    '''
    Socket: QTcpSocket = None
    Name: str
    Features: set
    Color: str = "255,255,255"
    IsReady: bool = False

    def __init__(self, socket: QTcpSocket, name="", features=set()):
        self.Socket = socket
        self.Name = name
        self.Features = features
