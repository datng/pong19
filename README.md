# Pong 2019
This is group 1's implementation of the Pong game for the *Internetkommunikation* class (Chair of Communication Networks, Department of Electrical and Computer Engineering, TU Munich) during the summer semester of 2019.

## Dependencies
- PyQt5 for networking

```
python3 -m pip install --user -U PyQt5
```

## Usage
The program is tested in this configuration:
- The server is run on Ubuntu 18.04
- The client can be on other linux machines or Windows.
### Server
The server is located in `source/server`
```
python3 p19_server.py
```

### Client
It is possible to run multiple clients from the same machine.

The client is located in `source/client`

```
python3 p19_client.py
```