# Server
- Die Implementierung des Server kann durch 3 große Aufgaben aufgeteilt werden:
  - Lobbynetzwerk (*Networking*)
  - Clientverwaltung (*Lobby*)
  - Individuelle Matchlogik (*Match*)
- Während der Entwicklung, da Server hochautomatisiert ohne Benutzereingabe funktionieren soll, wurde ein Automatisiertes Mock Client benutzt. Dies wird alle Befehle schicken, die in Lobby Protokol beschrieben sind, schicken, bis zum Match Start, und die Match Befehl vom UDP socket ausdrucken.
- Dies ermöchlicht uns, viele Fehler im Server zu erkennen, ohne ein funktionierende Client.
## Lobbynetzwerk (*Networking*)
- Das Protokol beschreibt die ganzen Netzbefehle, die durch Udp- und Tcp-Sockets beim Lobbynetzwerk geschrieben und gelesen werden. Für die zwei andere Teile gibt es viel Möglichkeit, einen "korrekten" Komponenten zu schreiben, und deshalb ist Lobbynetzwerk als erstes implementiert.
- PyQt wurde benutzt. Die Events connected/disconnected/readyRead/error sind nativ zu QTcpSocket, deshalb war es nicht nötig, die Verbindungen in einen Event Loop zu prüfen. Dies ermöglichte uns, die Verbindungen in einen abstrakteren Niveau zu verwalten.
- Es gibt in der Networking-Classe 3 Typen von Sockets.
  - Ein UdpSocket, das am Port 54000 gebunden ist, ist zuständig für die Broadcast von Clients.
  - Ein TcpSocket, das:
    - auf Verbindungen warten
    - alle Befehle, die durch Lobby Protocol definiert ist, zur Clientverwaltung weiterleiten
    - auf alle ungültige Befehle antworten mit `ERR_CMD_NOT_UNDERSTOOD`
    - die Signale von Clientverwaltung verwandeln in Netzwerkbefehle und zum richtigen Client schicken.
  - Eine Liste von Udp Sockets, nämlich Listeners, die verantwortlich sind für die gestartete Matches. Diese sind wiederverwendbar. Sobald ein Match zu Ende ist, wird der dazugehörige Listener befreit.
- Für die Lobbynetzwerk gibt es 3 Hilfklasse: MessageChecker, MessageBuilder, MessageParser.
  - Message Checker prüft ob ein vom Socket gelesene Befehl gültig ist (richtige Befehl, richtige Anzahl der mit Leerzeichen getrennten Elemente).
  - Message Builder baut ein Befehl auf durch die gegebene Parameter (strings, lists, sets, etc.)
  - Message Parser liest und sortiert die Messages in bearbeitbare Objekten, diese Objekten wird mithilfe der Events zum Clientverwaltung geschickt werden.
  - Diese Hilfklasse dienen dazu, dass es einfacher ist, den Ablauf vom Program zu verstehen, und die zusätzliche Protokol einzubauen.

## Clientverwaltung (*Lobby*)
- Clientverwaltung hat die Aufgabe, Clients zu verwalten. Nämlich:
  - Clients hinzufügen/löschen
  - Matches dynamisch erstellen/löschen
  - Fehler erkennen beim Matcherstellung/Teilnahme zum Client mitteilen
  - Matchergebnis von individuelle Matches zum Netzwerk zu schicken
  - Mithilfe der automatisierten Mock Clients ist es ziemlich einfach, Programmierfehler in dieser Klasse zu finden. Die meisten sind wegen Pythons fehlendes Typechecking und dynamisches Typesystem verursacht.

## Individuelle Matchlogik (Match)
- Für jeden Match gibt es zwei Client: ein Host und ein Guest. Die sind Referenzen zu Clients, die durch Clientverwaltung in eine Liste gespeichert sind. Wenn ein Client gelöscht wird, wird dessen zugehöriger Match auch gelöscht.
- Es gibt ein Problem von Identität im Match, nämlich: die Sockets, die `CREATE_MATCH` und `JOIN_MATCH` Befehle schicken, sind TcpSocket. Die Socket, die für die Aktualisierung der Bewegungen benutzt werden, sind aber Udp. Es gibt keine Möglichkeit, sicherzustellen, welche UdpSocket kommt vom gleichen Client als Tcp. Deshalb wurde extra Logik eingebaut für den Fall, dass Host und Guest aus gleichen Maschine ist. Der TCP-Befehl `I_AM_READY` und der erste UDP-Befehl soll nah nacheinander geschickt werden, damit die Mapping richtig funktionert.
- Diese Trick ermöglicht uns, mit einem Rechnern den Server mit bis zu 180 Clients zu testen (mehr gehts sowieso nicht, da es kein freie Listener mehr).
- Das Spiel ist mit einem Frequenz von 60fps aktualisiert. und die TCP-Aktualisierung werden mithilfe der Signal/Slot-Mechanismus zum Networking weitergeleitet. Die UDP-Aktualisierung sind einfach mit dem Listener im Match geschickt, ohne weitere Umleitung.