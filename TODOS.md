# Tasks to be done before 7.6.2019
## For client
- [ ] GUI (Michael & Benedikt)
  - [ ] Lobby (always in the beginning, and when match ends)
    - [ ] List available matches
    - [ ] Lets user choose one/create match and start match gui
    - [ ] (Everything without mouse for simplicity)
  - [ ] Match (when a match starts)
    - [ ] Can be blank at this phase
    - [ ] Go back to lobby when match ends
- [ ] Communication (Hien & Dat)
  - [ ] Broadcast with UDP to all servers on the network
  - [ ] Connect with the first server who answers to the broadcast
  - [ ] Send message to create match
  - [ ] Send message to join match

## For server
- [ ] Communication (Hien & Dat)
  - [ ] Answer to the broadcast
  - [ ] Keep a list of available clients
  - [ ] Send match_created back to first client
  - [ ] Send match_joined to second client
  - [ ] Send message to both clients that a match is started
  - [ ] Send message to both clients that the match ends

## For presentation
- [ ] Make the presentation (Corentin)